# WebSockets:

Nos permite comunicar de una forma muy versatil por ejemplo en frontend con el backend. El protocolo http es de comunicacion cliente-servidor. Siempre es el cliente el que inicia la comunicacion con el servidos, nunca al reves.

Con websockets si podemos hacer esto, el servidor iniciar comunicación con un cliente.

Es una tecnologia avanzada que hace posible abrir una sesion de comunicacion interactiva entre el navegador del usuario y un servidor. Tambien puede ser entre dos servidores.

- Es un protocolo estandar (RFC 6455).

- Esta diseñado para ser implementada en navegadores y servidores web, pero puede utilizarse por cualquier aplicación cliente/servidor.

- Usa los puertos habituales HTTO (80, 443).

- Atraviesa firewall y proxies.

Caso practicos donde usamos websockets:

- Juegos online multijugador.

- Aplicaciones de chat.

- Rotativos de información deportiva.

- Actualización en tiempo real de las actividades de tus amigos.

## Ejemplo de websockets en Node.js: Carpeta Ejemplos:

# websockets:

Le creamos su package.json:

```bash
npm init -y
```

Instalamos express:

```bash
npm i express
```

Y creamos un servidor de express:

## httpsServer.js:

```javascript
'use-strict';
const http = require('node:http');
const path = require('node:path');

const express = require('express');

const app = express();

app.use('/', (req, res, next) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

const server = http.createServer(app);

const PORT = 3001;

server.listen(PORT, () => {
  console.log(`server http on in http://localhost:${PORT}`);
});
```

Lo Arrancamos:

```bash
npx nodemon httpServer.js
```

Luego el profesor nos pasa un index.html, una plantilla. Lo tenemos en la carpeta.

🎯 Commit: Clase 5: Websockets, servidor http de express y plantilla html.

# Implementación de websocket:

Para esto vamos a instalar una libreria llamada socket.io:

```bash
npm repo socket.io
```

https://github.com/socketio/socket.io

Esta es una libreria muy versatil, se suele usar con muchos lenguajes de programación:

La instalamos:

```bash
npm i socket.io
```

Vamos a crear un nuevo fichero, lo llamamos webSocketsServer.js:

## webSocketsServer.js:

```javascript
// Cargamos la libreria:
const socketio = require('socket.io');

// exportamos una funcion que configura un servidor HTTP:
module.exports = (server) => {
  const io = socketio(server);

  // ante cada conexion de un cliente (socket)
  io.on('connection', (socket) => {
    console.log('Nueva conexion de un cliente, con el id, ', socket.id);
  });
};
```

Lo que hacemos en desde el archivo httpServer.js cargamos este nuevo servidor:

## webSocketsServer.js:

```javascript
const webSocketsServer = require('./webSocketsServer');

// Arrancamos el servidor de websocker: Le pasamos nuestro servidor:
webSocketsServer(server);
```

Luego en nuestro index.html, en el script ejecutamos la conexion:

```html
<script>
  $(function () {
    // Nos conectamos al servidor:
    const socket = io();
  });
</script>
```

Al refrescar vemos la respuesta por consola el log, ya hay conexion entre el servidor y el cliente, asi hay ya hay conexion abierta:

![](./img/socket.png)

Ahora lo que haremos es enviar al servidor un mensaje que pongamos en el input que tenemos en el index.html: Esta usando jquery:

## index.html

```html
<script>
  $(function () {
    // Nos conectamos al servidor:
    const socket = io();

    // Enviar mensaje al servidor: m, el id del input:
    $('form').submit(() => {
      const texto = $('#m').val();
      socket.emit('nuevo-mensaje', texto);
      $('#m').val(''); // limpio el input.
      return false;
    });
  });
</script>
```

Ahora en el servidor vamos a recibir ese mensaje:

## webSocketsServer.js:

```javascript
// Recibimos el mensaje
socket.on('nuevo-mensaje', (texto) => {
  console.log('Mensaje recibido de un cliente:', texto);
});
```

Ahora nos vamos al input y enviamos algo:

![](./img/socket1.png)

Ahora lo que hacemos es que el servidor responda al cliente:

## webSocketsServer.js:

```javascript
// Enviamos el mensaje a todos los sockets conectados: Osea al cliente
io.emit('mensaje-desde-el-servidor', texto);
```

Nos vamos al cliente para mostrar ese mensaje que nos envia el servidor:

## index.html

```html
<script>
  $(function () {
    // Recibir mensajes desde el servidor:
    socket.on('mensaje-desde-el-servidor', (texto) => {
      // Añadir el texto a la lista:
      const li = $('<li>').text(texto);
      $('#messages').append(li);
    });
  });
</script>
```

Vemos como se carga el mensaje en el html en el frontend. Ahora vamos a abrir otro cliente y podemos comunicarnos entre los dos. Como un chat:

![](./img/socket2.png)

🎯 Commit: Clase 5: Implementación de websocket, un chat.

# Servidor envia información a los clientes:

## webSocketServer.js

```javascript
// Servidor envia informacion:
setInterval(() => {
  socket.emit('noticia', 'noticia numero ' + Date.now());
}, 2000);
```

Y en el cliente escucharemos eso que emite el servidor:

## index.html:

```html
<script>
  // escuchamos lo que emite el servidor:
  socket.on('noticia', (texto) => {
    const textoCompleto = 'Nueva noticia:' + texto;
    // Añadir el texto a la lista:
    const li = $('<li>').text(textoCompleto);
    $('#messages').append(li);
  });
</script>
```

Recargamos y vemos como se muestra cada 2 segundos el mensaje en lo clientes. Aqui vemos como el servidor envia informacion al cliente.

![](./img/socket3.png)

🎯 Commit: Clase 5: Servidor envia información a clientes usando websocket.
