'use-strict';

// Cargamos la libreria amqplib:
const amqplib = require('amqplib');
// Cargamos la libreria:
const nodemailer = require('nodemailer');

//dotenv:
require('dotenv').config();

// definimos la cola:
const QUEUE = 'email-sender';

// Creamos una funcion main() y aqui la invocamos, como es asincrona lo hacemos asi:
main().catch((err) => console.log('Hubo un error: ', err));

async function main() {
  // Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
  const connection = await amqplib.connect(process.env.RABBITMQ_BROKER_URL);

  // Arrnacamos el transport:
  const transport = await createTransport();

  // Crear un canal:
  const canal = await connection.createChannel();

  // asegurarnos que existe la cola donde vamos a recibir los mensajes:
  await canal.assertQueue(QUEUE, {
    durable: true, // the queue will surviver broken restarts
  });

  // le decimos a la cola que esta trabajando para evitar saturacion: Lo hara de uno en uno. Dame un mensaje y hasta que no lo procese no me des mas. Recibe por parametros el numero de msj:
  canal.prefetch(1); // pending ack // Cuantos emails a la vez.

  // Ahora consumimos los mensajes:
  canal.consume(QUEUE, async (mensaje) => {
    const payload = JSON.parse(mensaje.content.toString());
    console.log(payload);

    // enviamos el email:
    const result = await transport.sendMail({
      from: process.env.EMAIL_SERVICE_FROM,
      to: payload.to,
      subject: payload.asunto,
      html: payload.cuerpo,
    });

    console.log(
      `URL de previsualización: ${nodemailer.getTestMessageUrl(result)}`
    );

    canal.ack(mensaje);
  });
}

// Funcion para crear un transporter:
async function createTransport() {
  // Entorno de desarrollo:
  // Creamos una cuenta de prueba:
  const testAccount = await nodemailer.createTestAccount();

  const developmentTransport = {
    host: testAccount.smtp.host,
    port: testAccount.smtp.port,
    secure: testAccount.smtp.secure,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  };

  return nodemailer.createTransport(developmentTransport);
}
