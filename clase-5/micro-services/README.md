Install dependencies:

```sh
npm install
```

- Start email sender service:

```sh
npm run email-sender-service
```
