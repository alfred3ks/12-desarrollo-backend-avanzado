# Microservicios:

# Implementar rabbit-mq en nuestra app:

Vamos a implementar microservicios en nuestra app nodeapp. Lo que haremos es encargarle a rabbit-mq el envio del correo cuando el usuario se loguea en la aplicacion.

Empezamos poniendo en las variables de entorno la configuracion para rabbit-mq:

## .env:

```
MONGODB_URI=mongodb://127.0.0.1/cursonode
JWT_SECRET=s876ads87dasuytasduytasd
EMAIL_SERVICE_FROM=admin@example.com
RABBITMQ_BROKER_URL=amqps://niidpjaa:qNsmSD5PmlyDjtJpozkfAVvUbtGOi5Q6@whale.rmq.cloudamqp.com/niidpjaa
```

Instalamos en el proyecto la libreria:

```bash
npm i amqplib
```

En nuestros modelos, tenemos el modelo de Usuario.js, vemos el metodo que envia el correo electronico al hacer login.

Hemos creado un módulo para cargar el microservicio que sera usado en el modelo, lo creamos en la carpeta lib:

## rabbitMQLib.js:

```javascript
'use-strict';

// Cargamos la libreria amqplib:
const amqplib = require('amqplib');

// Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
const canalPromise = amqplib
  .connect(process.env.RABBITMQ_BROKER_URL)
  .then((connection) => {
    // Crear un canal:
    return connection.createChannel();
  });

module.exports = canalPromise;
```

Vamos a crear un método para enviar correo electronico usando microservicios rabbitMQ:

## Usuario.js:

```javascript
// Método para enviar correo usando microservicios: Otro servicio (rabbitMQ) envia el email.
usuarioSchema.methods.sendEmailRabbitMQ = async function (asunto, cuerpo) {
  // Cargar módulo rabbitMQLib y enviamos un mensaje:
  const canal = await canalPromise;

  // asegurar que existe el exchange:
  const exchange = 'email-request';
  await canal.assertExchange(exchange, 'direct', {
    durable: true, //the exchange will survive broker restarts.
  });

  // publicar mensaje:
  const mensaje = {
    asunto,
    cuerpo,
  };

  canal.publish(exchange, '*', Buffer.from(JSON.stringify(mensaje)), {
    persistent: true, // the message will survive broker restarts
  });
};
```

Ahora este método sendEmailRabbitMQ lo usaremos en el controlador LoginController.js:

## LoginController.js:

```javascript
// Llamamos el metodo para mandar el email usando microservicios:
usuario.sendEmailRabbitMQ('Bienvenido', 'Bienvenido a NodeApp');
```

Ahora nos autenticamos y vemos como en rabbit-mq en la interface web ya se nis ha creado el exchange:

![](./img/rabbitMQWeb.png)

La primera parte la tenemos. Tenemos ya creado en envio, lo que llamamos los publish.

## Microservicios:

Ahora lo que haremos es conectar nuestra aplicacion a un microservicio, esto es una aplicación aparte.

Esto lo vamos a crear en una carpeta aparte nunca suele estar dentro de nuestra aplicacion. La llamaremos micro-services. Ahi es donde vamos a meter el consumidor de los mensajes.

NOTA: Recordar esto deberia hacerse por separado. Aunque en clase el lo ha puesto dentro de nodeapp.

Nos vamos a crear un archivo emailSender.js es el mismos constenido de consumer.js que vimos en la clase 4. Le haremos unos retoques.

Instalamos dotenv y amqplib:

```bash
npm i dotenv amqplib
```

## emailSender.js:

```javascript
'use-strict';

// Cargamos la libreria amqplib:
const amqplib = require('amqplib');

//dotenv:
require('dotenv').config();

// definimos la cola:
const QUEUE = 'email-sender';

// Creamos una funcion main() y aqui la invocamos, como es asincrona lo hacemos asi:
main().catch((err) => console.log('Hubo un error: ', err));
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

async function main() {
  // Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
  const connection = await amqplib.connect(process.env.RABBITMQ_BROKER_URL);

  // Crear un canal:
  const canal = await connection.createChannel();

  // asegurarnos que existe la cola donde vamos a recibir los mensajes:
  await canal.assertQueue(QUEUE, {
    durable: true, // the queue will surviver broken restarts
  });

  // le decimos a la cola que esta trabajando para evitar saturacion: Lo hara de uno en uno. Dame un mensaje y hasta que no lo procese no me des mas. Recibe por parametros el numero de msj:
  canal.prefetch(1); // pending ack // Cuantos emails a la vez.

  // Ahora consumimos los mensajes:
  canal.consume(QUEUE, async (mensaje) => {
    const payload = mensaje.content.toString();
    console.log(payload);
    await sleep(150);
    canal.ack(mensaje);
  });
}
```

Vamos a arrancar por consola este microservicio. Nos creamos en el package.json un script de arranque:

```json
"email-sender-service": "node ./emailSender.js"
"dev": "npx nodemon ./emailSender.js"
```

```bash
npm run email-sender-service
```

Lo Arrancamos y vamos al servicio de rabbitMQ y vemos si nos has creado la cola. Queues and Streams:

![](./img/rabbitMQWeb1.png)

Ahora lo que haremos es hacer el binding del Exchange: Entremos en email-sender, nos vamos a donde dice Bindings:

![](./img/rabbitMQWeb2.png)

Le damos a Bind.

![](./img/rabbitMQWeb3.png)

Ahora vamos y nos loguemos de nuevo en la nodeapp y vemos a ver si le llegan los mensajes. Vemos que el servicio web se ha generado el mensaje y por consola tambien:

![](./img/rabbitMQWeb4.png)

![](./img/msjconsole.png)

Ahora vamos a hacer que envie un email. Lo que hacemos es modificar nuestro archivo emailSender.js, le vamos a agregar un transport que tenemos en nodeapp en lib/emailTransportConfigation.js le hacemos unas mejoras.

Debemos instalar nodemailer en nuestro micro-services:

```bash
npm i nodemailer
```

Nos quedaria asi nuestro archivo emailSender.js del micro-service:

## emailSender.js:

```javascript
'use-strict';

// Cargamos la libreria amqplib:
const amqplib = require('amqplib');
// Cargamos la libreria:
const nodemailer = require('nodemailer');

//dotenv:
require('dotenv').config();

// definimos la cola:
const QUEUE = 'email-sender';

// Creamos una funcion main() y aqui la invocamos, como es asincrona lo hacemos asi:
main().catch((err) => console.log('Hubo un error: ', err));

async function main() {
  // Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
  const connection = await amqplib.connect(process.env.RABBITMQ_BROKER_URL);

  // Arrnacamos el transport:
  const transport = await createTransport();

  // Crear un canal:
  const canal = await connection.createChannel();

  // asegurarnos que existe la cola donde vamos a recibir los mensajes:
  await canal.assertQueue(QUEUE, {
    durable: true, // the queue will surviver broken restarts
  });

  // le decimos a la cola que esta trabajando para evitar saturacion: Lo hara de uno en uno. Dame un mensaje y hasta que no lo procese no me des mas. Recibe por parametros el numero de msj:
  canal.prefetch(1); // pending ack // Cuantos emails a la vez.

  // Ahora consumimos los mensajes:
  canal.consume(QUEUE, async (mensaje) => {
    const payload = JSON.parse(mensaje.content.toString());
    console.log(payload);

    // enviamos el email:
    const result = await transport.sendMail({
      from: process.env.EMAIL_SERVICE_FROM,
      to: payload.to,
      subject: payload.asunto,
      html: payload.cuerpo,
    });

    console.log(
      `URL de previsualización: ${nodemailer.getTestMessageUrl(result)}`
    );

    canal.ack(mensaje);
  });
}

// Funcion para crear un transporter:
async function createTransport() {
  // Entorno de desarrollo:
  // Creamos una cuenta de prueba:
  const testAccount = await nodemailer.createTestAccount();

  const developmentTransport = {
    host: testAccount.smtp.host,
    port: testAccount.smtp.port,
    secure: testAccount.smtp.secure,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  };

  return nodemailer.createTransport(developmentTransport);
}
```

En el modelo de usuario, dentro de nodeapp hacemos una modificación, le agregamos al mensaje to: this.email:

## Usuario.js:

```javascript
// publicar mensaje:
const mensaje = {
  asunto,
  to: this.email,
  cuerpo,
};
```

Hacemos login y vemos la salida por consola:

![](./img/msjconsole1.png)

Funciona perfectamente. Y vemos que si el microservice falla nuestra app no falla porque son independientes.

Tenemos la url de previsuelización:

https://ethereal.email/message/ZZ2Q271VE-Mlh6GQZZ2RAPruXtLvm9NxAAAAAUz9wR9CcQcsOKRB1PrMpOY

Aqui podemos ver el correo.

![](./img/rabbitMQWeb5.png)

🎯 Commit: Clase 5: Implementar micro servicios a nodepop.
