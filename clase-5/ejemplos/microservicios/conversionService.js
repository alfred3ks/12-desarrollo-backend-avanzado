'use-strict';

// microservicio de conversion de moneda:
const { Responder } = require('cote');

// almacen de datos:
const tasas = {
  USD_EUR: 0.94,
  EUR_USD: 1.06,
};

// Lógica del servicio:
const responder = new Responder({ name: 'servicio-de-moneda' });

// Creamos un escuchador del evento: Nos suscribimos al evento convertir-moneda:
responder.on('convertir-moneda', (req, done) => {
  const { cantidad, desde, hacia } = req;
  console.log(Date.now(), 'servicio:', cantidad, desde, hacia);

  // calcular la tasa de cambio: tasas[USD_EUR]
  const tasa = tasas[`${desde}_${hacia}`];
  const resultado = cantidad * tasa;
  done(resultado);
});
