'use-strict';

// esta app necesita un microservicio para hacer cambios de moneda:

const { Requester } = require('cote');

const requester = new Requester({ name: 'app' });

// Creamos un evento:
const evento = {
  type: 'convertir-moneda',
  cantidad: 100,
  desde: 'USD',
  hacia: 'EUR',
};

// Creamos un bucle para lanzar evento cada cierto tiempo: Para ver el sistema de colas: si arranca
setInterval(() => {
  // Lanzamos este evento que es recogido por convertionService.js ya que tiene el responder.on():
  requester.send(evento, (resultado) => {
    console.log(
      Date.now(),
      'app obtiene resultado de la conversion:',
      resultado
    );
  });
}, 1000);
