// Cargamos la libreria:
const socketio = require('socket.io');

// exportamos una funcion que configura un servidor HTTP:
module.exports = (server) => {
  const io = socketio(server);

  // ante cada conexion de un cliente (socket)
  io.on('connection', (socket) => {
    console.log('Nueva conexion de un cliente, con el id, ', socket.id);

    // Recibimos el mensaje del cliente
    socket.on('nuevo-mensaje', (texto) => {
      console.log('Mensaje recibido de un cliente:', texto);

      // Enviamos el mensaje a todos los sockets conectados: Osea al cliente
      io.emit('mensaje-desde-el-servidor', texto);
    });

    // Servidor envia informacion:
    setInterval(() => {
      socket.emit('noticia', 'noticia numero ' + Date.now());
    }, 2000);
  });
};
