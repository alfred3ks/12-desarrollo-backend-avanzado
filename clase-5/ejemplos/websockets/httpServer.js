'use-strict';
const http = require('node:http');
const path = require('node:path');

const express = require('express');
const webSocketsServer = require('./webSocketsServer');

const app = express();

app.use('/', (req, res, next) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

const server = http.createServer(app);

const PORT = 3001;

server.listen(PORT, () => {
  console.log(`server http on in http://localhost:${PORT}`);
});

// Arrancamos el servidor de websocker: Le pasamos nuestro servidor:
webSocketsServer(server);
