# Microservicios: Inicio del descanso video 5.

La arquitectura de microservicios nace como respuesta a la problematica del mantenimiento y evolución de grandes aplicaciones monolíticas.

Si queremos leer a un experto en microservicios tenemos a James Lewis. Arquitecturas y diseños de plataformas.

Diferencias entre una app monolitica y una con microservicios:

![](./img/microservicios.png)

## Problemas tipocos que pueden aparecer en una app monolitica:

• Si algo falla en la aplicación, todo el sistema se cae.

• Son difícil de escalar a nivel de infraestructura.

• Es difícil de escalar a nivel de equipo de desarrollo (hasta que todo los desarrolladores saben cómo funciona todo el monolito).

• Es muy difícil mantener la interdependencia de relaciones y un código limpio (spaguetti code, merge conflicts, etc…).

• Es la manera natural de desarrollar (todo software tiende a monolito).

## Ventajas de los microservicios:

• Si falla un microservicio, no todo el sistema falla.

• Su infraestructura es fácilmente escalable.

• Fácil de escalar los equipos de desarrollo (cada equipo sólo debe mantener un par de microservicios).

• Código más mantenible y reutilizable (menos interdependencias).

• Permiten utilizar la mejor tecnología para cada problema.

• Facilitan externalización a equipos de desarrollo de otras empresas.

## Comparacion de monolitoco y microservicios:

![](./img/microservicios1.png)

## Problematica de los microservicios:

• Orquestación: es más difícil hacer una buena coordinación de todos.

• Entorno de desarrollo: arrancar un entorno de desarrollo con 300 microservicios te puede llevar un rato…solución: Docker/Vagrant.

• Negociación y compromiso: cada micro servicio debe mantener sus contratos con otros micro servicios que los usan (no podemos cambiar cosas a la ligera).

• Gestión de logs. Están distribuidos, hay que unificarlos para facilitar la búsqueda.

• Unión de datos: los JOIN debemos hacerlos a mano.

• ¿Quién es el responsable de controlar la autenticación y autorización?.

Para ver al detalle tenemos la información en el pdf de la clase.

## Como pasar de monolito a microservicios:

![](./img/microservicios2.png)

## Caracteristicas de un microservicio:

• Resolver un sólo problema y hacerlo muy bien. Filosofia de linux.

• Utilizar la mejor tecnología posible para resolver ese problema.

• Exponer un API con endpoints para interactuar con él.

• Base de datos propia (a ser posible, se aceptan BD compartidas).

• Conexión al bus de mensajes/cola de eventos (si es necesario).

• Librería cliente del API (a ser posible).

El ejemplo que tenemos de rabbitMQ se asemeja bastante a un microservicio, ahora vamos a hacer otro.

## Creacion de un microservicio:

Vamos a usar una libreria llamada cote:

```bash
npm repo cote
```

https://github.com/dashersw/cote

Esta una libreria para crear un microservicio con cero configuración.

Dentro de la carpeta ejemplo crearemos uno. Creamos la carpeta microservicios.

Creamos el package.json:

```bash
npm init -y
```

Instalamos cote:

```bash
npm i cote
```

Nos creamos un archivo llamado app.js

## app.js:

```javascript
'use-strict';

// esta app necesita un microservicio para hacer cambios de moneda:

const { Requester } = require('cote');

const requester = new Requester({ name: 'app' });

// Creamos un evento:
const evento = {
  type: 'convertir-moneda',
  cantidad: 100,
  desde: 'USD',
  hacia: 'EUR',
};

// Lanzamos este evento que es recogido por convertionService.js ya que tiene el responder.on():
requester.send(evento, (resultado) => {
  console.log(Date.now(), 'app obtiene resultado:', resultado);
});
```

La arrancamos:

```bash
npx nodemon app.js
```

Ahora creamos otro fichero:

## conversionService.js

```javascript
'use-strict';

// microservicio de conversion de moneda:
const { Responder } = require('cote');

// almacen de datos:
const tasas = {
  USD_EUR: 0.94,
  EUR_USD: 1.06,
};

// Lógica del servicio:
const responder = new Responder({ name: 'servicio-de-moneda' });

// Creamos un escuchador del evento: Nos suscribimos al evento convertir-moneda:
responder.on('convertir-moneda', (req, done) => {
  const { cantidad, desde, hacia } = req;
  console.log(Date.now(), 'servicio:', cantidad, desde, hacia);

  // calcular la tasa de cambio: tasas[USD_EUR]
  const tasa = tasas[`${desde}_${hacia}`];
  const resultado = cantidad * tasa;
  done(resultado);
});
```

Tambien lo arrancamos:

```bash
npx nodemon conversionService.js
```

Ahora vemos como en la consola de app.js vemos el resultado de converstir 199 euros a dolares.

## app.js:

```javascript
// Creamos un bucle para lanzar evento cada cierto tiempo: Para ver el sistema de colas: si arranca
setInterval(() => {
  // Lanzamos este evento que es recogido por convertionService.js ya que tiene el responder.on():
  requester.send(evento, (resultado) => {
    console.log(
      Date.now(),
      'app obtiene resultado de la conversion:',
      resultado
    );
  });
}, 1000);
```

Ahora en app.js le agregamos un bucle para que cada segundo haga una conversion, si arancamos dos conversionService.js lo que pasa es que se balancea la carga una peticion va a uno y otra a otro. Ahora si paro ambos conversionService.js y dejo arrancado app.js se van encolando las peticiones y al volver a arrancar conversionService.js estas se procesan todas de golpe.

Como vemos es muy facil implementar microservicios usando esta libreria.

🎯 Commit: Clase 5: Microservicios, libreria cote.
