/*
Creamos nuestros controladores con clases:
*/
const Usuario = require('../models/Usuario');
const createError = require('http-errors');
class PrivateController {
  async index(req, res, next) {
    try {
      // obtener el id del usuario de la sesion
      const usuarioId = req.session.usuarioLogueado;

      // buscar el usuario en la bd:
      const usuario = await Usuario.findById(usuarioId);

      if (!usuario) {
        next(createError(500, 'usaurio no encontrado'));
        return;
      }
      // Renderizamos la vista y la variable local
      res.render('private', { email: usuario.email });
    } catch (err) {
      next(err);
    }
  }
}

module.exports = PrivateController;
