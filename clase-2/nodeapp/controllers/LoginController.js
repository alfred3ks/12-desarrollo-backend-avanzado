/*
Controlador para la página de login:
*/

// Cargamos el modelo:
const Usuario = require('../models/Usuario');

class LoginController {
  // Método que carga el formulario:
  index(req, res, next) {
    // Inicializamos la variable local de error y email a vacio para que no pete la app:
    res.locals.error = '';
    res.locals.email = '';
    res.render('login');
  }

  // Método que comprueba el usuario al enviar el formulario:
  async post(req, res, next) {
    // console.log(req.body)
    try {
      // Extraemos los datos del body:
      const { email, password } = req.body;

      // Comprobamos que exista en bd el usuario:
      const usuario = await Usuario.findOne({
        email: email,
      });

      // Si no lo encuentra o contraseña mal retorno -> error:
      if (!usuario || !(await usuario.comparePassword(password))) {
        // Respondemos con una variable que tenemos en la vista y multiidioma:
        res.locals.error = req.__('Invalid credentials');

        // Enviamos ala vista el email:
        res.locals.email = email;

        // Renderizamos de nuevo la vista:
        res.render('login');
        return;
      }

      // apuntar a la sesion de usuario, que esta autenticado
      req.session.usuarioLogueado = usuario._id; // Este es el id de la bd.
      // Si lo encuentra y la contraseña bien -> redirigimos a zona privada:
      res.redirect('/private');
    } catch (err) {
      next(err);
    }
  }
}

module.exports = LoginController;
