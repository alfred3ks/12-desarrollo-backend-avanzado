# Clase 2: Autenticación: Min 0:30:29

En la parte de fundamentos vimos una autenticación con basic auth. Esa es una forma muy básica y poco versatil pero es una forma de hacerlo.

Existen muchas formas de autenticacion pero veremos las mas habituales y mas importantes conocer.

## Autenticacion por https: Ver pdf:

El protocolo HTTP es un protocolo sin estado: cada petición del navegador a un servidor es independiente de las anteriores.
Sin embargo en las aplicaciones web a veces necesitamos mantener el estado para poder relacionar peticiones.

Ejemplo:

• Petición 1: el usuario añade un producto al carrito de compra.

• Petición 2: el usuario va a pagar en el proceso de checkout

Existen formas para mantener ese estado que necesitamos:

## Autenticación por sesion: Ver pdf: Las cookies:

![](./img/sesion.png)

Un usuario(user) con su pc que quiere hacer login en un website, este hace una peticion con unas credenciales(credentials) al servidor(server), el servidor valida que esas credenciales son correctas y existe y crea en su memoria una sesion, le habilita un espacio para guardar la informacion sobre ese usuario. Ese espacio de sesion tiene un identificador, un id(ID). Este id se lo devuelve al usaurio con una cookie con ese id. OJO debemos saber que la sesion vive del lado del servidor, este es el que sabe lo que hay en esa sesion pero no se la da al cliente, solo le da el id dentro de una cookie.

La proxima ves que me hagas una peticion(request) me la haces con esa cookie con es id para que yo sepa cual es tu caja o espacio, el servidor con ese id busca ese espacio y en funcion del rol del usuario le devuelve los datos que ha solicitado.

La autenticación por sesión se basa en el uso de cookies.
Las cookies son bloques de información enviados en las cabeceras HTTP en las respuestas de los servidores a los navegadores.
Los navegadores almacenan estos bloques de información y los envían automáticamente a los servidores en las siguientes peticiones.

A continuación vemos mas o menos como seria el proceso de peticiones:

![](./img/sesion1.png)

## Anatomia de una cookie:

Las cookies están compuestas por los siguientes elementos:

• domain: dominio del servidor que ha puesto la cookie.

• path: ruta opcional de la página que ha puesto la cookie. Si existe,
sólo se enviará la cookie de vuelta a esa ruta.

• expires: fecha de expiración de la cookie.

• secure: flag opcional para indicar que la cookie sólo se enviará por protocolo HTTPS.

• httpOnly: flag opcional que indica que solo la puede leer el servidor.

• value: separados por ; pueden tener nombre.

A continuacion vemos como se veria una cookie:

```
Set-Cookie: name=darth; role=admin;
domain=deathstar.com; path=/home; secure;
expires=Tue, 29 Jan 1985 01:02:03 GMT+2
```

## Vamos a implementar en nuestra app una auenticación con sesion: 0:54:04

Para esto tenemos en nuestra app el boton de login.
Vamos a crearnos un modelo para crear usuarios que guardaremos en la BD. Vamos a la carpeta models:

models/Usuario.js:

## Usuario.js:

```javascript
const mongoose = require('mongoose');

// creamos esquema:
const usuarioSchema = mongoose.Schema({
  email: String,
  password: String,
});

// creamos el modelo:
const Usuario = mongoose.model('Usuario', usuarioSchema);

// exportamos el modelo
module.exports = Usuario;
```

Ahora los que veremos como podemos importar varios modelos en un solo require:

Ahora dentro de la carpeta models tiene que haber un fichero index.js, el cual creamos:

## models/index.js

```javascript
// Forma de exportar varios modelos usando un archivo index.js:
module.exports = {
  Agente: require('./Agente'),
  Usuario: require('./Usuario'),
};
```

Ahora vamos a ir a init-db.js y vamos a crear unos usuario iniciales:

## init-db.js:

```javascript
// Asi cargamos todos los modelos en un solo require:
const { Agente, Usuario } = require('./models');
```

Dentro de las funcion main() que usamos para arrancar los agentes la usamos y creamos tambien usuarios:

```javascript
async function main() {
  // espero a que se conecte a la base de datos
  await new Promise((resolve) => connection.once('open', resolve));

  const borrar = await pregunta(
    'Estas seguro de que quieres borrar la base de datos y cargar datos iniciales?'
  );
  if (!borrar) {
    process.exit();
  }

  // inicializar la colección de agentes
  await initAgentes();
  // inicializar la colección de usuarios
  await initUsuarios();

  connection.close();
}
```

Lo podemos ver en la funcion initUsuarios(), esta funcion la tenemos que crear, es practicamente igual a la de crear Agentes, la vemos mas abajo:

```javascript
// Funcion que crea usuarios en la BD:
async function initUsuarios() {
  // eliminar
  const deleted = await Usuario.deleteMany();
  console.log(`Eliminados ${deleted.length} usuarios.`);

  // crear
  const inserted = await Usuario.insertMany(initData.usuarios);
  console.log(`Creados ${inserted.length} usuarios.`);
}
```

Como vemos aqui estamos usando un .json que tiene esos datos es el archivo init-db-data.json, dentro del objeto que tenemos agregamos los usuario de arranque:

## init-db-data.json:

```json
{
  "agentes": [
    {
      "name": "Smith",
      "age": 33
    },
    {
      "name": "Jones",
      "age": 23
    },
    {
      "name": "Brown",
      "age": 46
    }
  ],
  "usuarios": [
    {
      "email": "admin@example.com",
      "password": "1234"
    },
    {
      "email": "usuario1@example.com",
      "password": "1234"
    }
  ]
}
```

Ahora vamos a probar si se insertan esos datos, paramos la app y arrancamos el siguiente comando para insertar data de inicio en la bd:

```bash
npm run init-db
```

Nos reguntara por consola y debemos responder. Responderemos en español: si. ahora revisamos la BD y vemos si se han creado correctamente:

![](./img/user-mongo.png)

🎯 Commit: Clase 2: Autenticación y creación de usuarios como data inicial al arrancar la app.

Ya tenemos usuario ahora vamos a realizar una pagina de login:

## Página de Login:

Vamos a la carpeta controller y ahi creamos el controlador de la página de login.

controller/LoginController.js.

```javascript
/*
Controlador para la página de login:
*/
class LoginController {
  index(req, res, next) {
    res.render('login');
  }
}

module.exports = LoginController;
```

Ahora vamos a app.js y lo colocamos ahi, vamos a las rutas del website:

## app.js:

```javascript
// 📌 Cargamos los controladores:
const LoginController = require('./controllers/LoginController');

/**
 * Rutas del website
 */
// 📌 Creamos una instancia del controlador:
const loginController = new LoginController();

// Ruta para el login:
app.get('/login', loginController.index);
```

Ahora vamos a crear la vista de esa página, eso lo haremos en la carpeta views:

views/login.ejs

Esto es una copia de feature. Como la plantilla que estamos usando es de bootstrap, vamos a la web de bootstrap y descargamos el código de una plantilla para el formulario de login.

https://getbootstrap.com/docs/5.3/forms/overview/

Cargamos la pagina con la ruta http://localhost:3000/login y vemos que funciona.

🎯 Commit: Clase 2: Autenticación creación de la página con bootstrap del login.

Ahora lo que haremos es preparar nuestro formulario porque esta incompleto, le falta el metodo, y el name de los input.

Nos vamos al LoginController.js para manejar esos datos del formulario, para eso creamos un nuevo metodo para el post.

## LoginCotroller.js:

```javascript
/*
Controlador para la página de login:
*/
class LoginController {
  index(req, res, next) {
    res.render('login');
  }
  post(req, res, next) {
    console.log(req.body);
    res.send('data');
  }
}

module.exports = LoginController;
```

Ahora en app.js definimos esa ruta:

## app.js:

```javascript
// Ruta para el login:
app.post('/login', loginController.post);
```

Lo probamos y vemos que funciona, por conola obtenemos los datos del formulario que vienen del body. Ahora los que haremos es tomar esa informacion que nos viene por el body

Nuestro loginController nos queda asi:

```javascript
/*
Controlador para la página de login:
*/

// Cargamos el modelo:
const Usuario = require('../models/Usuario');

class LoginController {
  // Método que carga el formulario:
  index(req, res, next) {
    // Inicializamos la variable local de error:
    res.locals.error = '';
    res.render('login');
  }

  // Método que comprueba el usuario al enviar el formulario:
  async post(req, res, next) {
    // console.log(req.body)

    try {
      // Extraemos los datos del body:
      const { email, password } = req.body;

      // Comprobamos que exista en bd el usuario:
      const usuario = await Usuario.findOne({
        email: email,
      });
      // Si no lo encuentra o contraseña mal retorno -> error:
      if (!usuario || usuario.password !== password) {
        // Respondemos con una variable que tenemos en la vista y multiidioma:
        res.locals.error = req.__('Invalid credentials');
        // Renderizamos de nuevo la vista:
        res.render('login');
        return;
      }
      // Si lo encuentra y la contraseña bien -> redirigimos a zona privada:
      res.redirect('/private');
    } catch (err) {
      next(err);
    }
  }
}

module.exports = LoginController;
```

Y en la vista hemos colocado la variable local para mostrar el error si el usuario no es correcto, lo vemos en view/login.ejs:

```html
<!-- Pintamos la variable global de error si las credenciales son incorrectas: -->
<p><%= error%></p>
```

Es de destacar que debemos inicializar a vacio esta variable local cuando cargamos por primera vez el login, eso lo vemos en el método index del controlador.

Ahora lo que haremos es pulir un poco mas el formulario, si las credenciales no son validas vamos a mantener el correo, que se quede en el formulario, la contraseña no.

Para eso en la vista vamos a crear una variable local para el email. Eso lo haremos con el atributo value. value="<%= email %>"

```html
<input
  type="email"
  value="<%= email %>"
  name="email"
  class="form-control"
  id="exampleInputEmail1"
  aria-describedby="emailHelp"
/>
```

Ahora nos vamos al controlador LoginController.js y cuando se renderice por primera vez tambien se renderice a vacio esa varaible local sino peta la app.

Asi queda el controlador:

```javascript
/*
Controlador para la página de login:
*/

// Cargamos el modelo:
const Usuario = require('../models/Usuario');

class LoginController {
  // Método que carga el formulario:
  index(req, res, next) {
    // Inicializamos la variable local de error y email a vacio para que no pete la app:
    res.locals.error = '';
    res.locals.email = '';
    res.render('login');
  }

  // Método que comprueba el usuario al enviar el formulario:
  async post(req, res, next) {
    // console.log(req.body)

    try {
      // Extraemos los datos del body:
      const { email, password } = req.body;

      // Comprobamos que exista en bd el usuario:
      const usuario = await Usuario.findOne({
        email: email,
      });
      // Si no lo encuentra o contraseña mal retorno -> error:
      if (!usuario || usuario.password !== password) {
        // Respondemos con una variable que tenemos en la vista y multiidioma:
        res.locals.error = req.__('Invalid credentials');

        // Enviamos ala vista el email:
        res.locals.email = email;

        // Renderizamos de nuevo la vista:
        res.render('login');
        return;
      }
      // Si lo encuentra y la contraseña bien -> redirigimos a zona privada:
      res.redirect('/private');
    } catch (err) {
      next(err);
    }
  }
}

module.exports = LoginController;
```

Ahora lo que haremos es que en nuestro schema del modelo de creacion de usuario estos sean unicos, vamos a hacer una modificación, el el Schema agremamos la propiedad unique:true.

## models/Usuario.js:

```javascript
const mongoose = require('mongoose');

// creamos esquema:
const usuarioSchema = mongoose.Schema({
  email: { type: String, unique: true },
  password: String,
});

// creamos el modelo:
const Usuario = mongoose.model('Usuario', usuarioSchema);

// exportamos el modelo
module.exports = Usuario;
```

Con esto ya nos garantizamos que si un email ya existe en la bd no se pueda volver a crear otro usuario con ese mismo email. Esto esta bien para cuando tengamos el formulario de creación de usuarios.

🎯 Commit: Clase 2: Autenticación, se agrega la lógica para validar un usuario con la bd con los datos del formulario.

Ahora vamos a crear esa pagina de private que hemos redireccionar al hacer login el usuario.

Para esto vamos a crear un nuevo controlador, nos vamos a controller:

controller/PrivateController.js

## PrivateController.js:

```javascript
/*
Creamos nuestros controladores con clases:
*/
class PrivateController {
  index(req, res, next) {
    res.render('private');
  }
}

module.exports = PrivateController;
```

Ahora en app.js creamos la ruta:

## app.js:

```javascript
// 📌 Cargamos los controladores:
const PrivateController = require('./controllers/PrivateController');

/**
 * Rutas del website
 */
// 📌 Creamos una instancia del controlador:
const privateController = new PrivateController();

// Ruta para la pagina private:
app.get('/private', privateController.index);
```

Ahora vamos y creamos esa vista en view/private.ejs.

🎯 Commit: Clase 2: Autenticación, se agrega la página de private junto con el controlador.

Ahora veamos que esta página no es como tal privada, es simplemente que al hacer login se redirige a esa página. Ahora lo que haremos que solo la puedan ver los usuarios que estan logueados y si no esta logueado se redirige al login.

## Proteger ruta: Página private:

Lo que haremos es guardar en la sesion que el usuario esta logueado. Para manejar la sesion lo haremos en app.js y nos vamos a apoyar en una libreria. Esta libreria nos va a ayudar con esa caja de sesion que hemos visto antes.

Para ver la libreria:

```bash
npm repo express-session
```

https://github.com/expressjs/session

Esto es un middeleware para gestionar sesiones en express.

La instalamos:

```bash
npm install express-session
```

Ahora vamos a app.js:

## app.js:

```javascript
// Cargamos la libreria session:
const session = require('express-session');

// Cargamos el middleware de session:
app.use(
  session({
    name: 'nodeapp-session', // nombre de la cookie
    secret: 'asanjnvvldfngfsdljvfdjgfdg', // pisada de gato, esto es cualquier valor.
    saveUninitialized: true, // Forces a session that is "uninitialized" to be saved to the store.
    resave: false, // Forces the session to be saved back to the session store, even if the session was never modified during the request.
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 2, // Tiempo de expiración (ms) 2 dias, de la sesion por inactividad.
    },
  })
);
```

Ahora cuando el usuario hace login, eso lo vemos en el controlador de LoginController.js:

```javascript
/*
Controlador para la página de login:
*/

// Cargamos el modelo:
const Usuario = require('../models/Usuario');

class LoginController {
  // Método que carga el formulario:
  index(req, res, next) {
    // Inicializamos la variable local de error y email a vacio para que no pete la app:
    res.locals.error = '';
    res.locals.email = '';
    res.render('login');
  }

  // Método que comprueba el usuario al enviar el formulario:
  async post(req, res, next) {
    // console.log(req.body)

    try {
      // Extraemos los datos del body:
      const { email, password } = req.body;

      // Comprobamos que exista en bd el usuario:
      const usuario = await Usuario.findOne({
        email: email,
      });
      // Si no lo encuentra o contraseña mal retorno -> error:
      if (!usuario || usuario.password !== password) {
        // Respondemos con una variable que tenemos en la vista y multiidioma:
        res.locals.error = req.__('Invalid credentials');

        // Enviamos ala vista el email:
        res.locals.email = email;

        // Renderizamos de nuevo la vista:
        res.render('login');
        return;
      }

      // apuntar a la sesion de usuario, que esta autenticado
      req.session.usuarioLogueado = usuario._id; // Este es el id de la bd.
      // Si lo encuentra y la contraseña bien -> redirigimos a zona privada:
      res.redirect('/private');
    } catch (err) {
      next(err);
    }
  }
}

module.exports = LoginController;
```

Aqui hemos agregado esto:

```javascript
// apuntar a la sesion de usuario, que esta autenticado
req.session.usuarioLogueado = usuario._id; // Este es el id de la bd.
```

Ahora vamos al controladore que renderiza la vista y antes de renderizarla debemos comprobar la sesion:

## PrivateController.js:

```javascript
/*
Creamos nuestros controladores con clases:
*/
class PrivateController {
  index(req, res, next) {
    // si el cliente que hace la peticion no tiene en su sesion la variable usuarioLogueado le mandamos al login.
    if (!req.session.usuarioLogueado) {
      res.redirect('/login');
      return;
    }

    res.render('private');
  }
}

module.exports = PrivateController;
```

Comprobamos que tenemos una cookie por medio de las devtools del navegador.

Descanso... 21:35 vuelve.

🎯 Commit: Clase 2: Descanso, autenticación, se protege con session la página private.

## Creación de módulo para paginas que requieren que el usuario este logueado:

Lo que haremos ya que necesitaremos en mas páginas loguearnos y volver a utilizar la comprobación si el usuario esta logueado es crear un módulo a parte. El código a reutilizar es el que tenemos en controller/PrivateController.js

```javascript
// si el cliente que hace la peticion no tiene en su sesion la variable usuarioLogueado le mandamos al login.
if (!req.session.usuarioLogueado) {
  res.redirect('/login');
  return;
}
```

El módulo lo vamos a crear en lib:

## sessionAuthMiddleware.js:

```javascript
// modulo que esporta un middleware que controla si estamos logueados o no.

module.exports = (req, res, next) => {
  // si el cliente que hace la peticion no tiene en su sesion la variable usuarioLogueado le mandamos al login.
  if (!req.session.usuarioLogueado) {
    res.redirect('/login');
    return;
  }
  next();
};
```

Ahora este middleware lo vamos a usar en app.js:

## app.js:

```javascript
// Agrupamos los middleware:
const sessionAuthMiddleware = require('./lib/sessionAuthMiddleware');

// Ruta para la pagina private:
app.get('/private', sessionAuthMiddleware, privateController.index);
```

Vemos como protegemos con ese middleware la pagina de privado en la ruta. Esto es como lo hicimos en la api para proteger la rutas. Lo probamos y vemos que funciona perfectamente.

🎯 Commit: Clase 2: Autenticación, se crea middleware de session para reutilizar el código.

## Solución de la contraseña en claro: Cifrado de contraseñas: Seguridad en el diseño:

Vamos a ver como podemos cifrar las contraseñas actualmente las contraseñas estan en claro, los usuarios que hemos creado al arancar la app estan en claro.

```json
"usuarios": [
    {
      "email": "admin@example.com",
      "password": "1234"
    },
    {
      "email": "usuario1@example.com",
      "password": "1234"
    }
  ]
```

Tenemos la siguiente web para leer, nos servira para poder saber como lmacenar un password de manera segura.

https://codahale.com/how-to-safely-store-a-password/

Para el cifrado de la contraseña vamos a usar bcrypt. Es una libreria no solo usada en nodejs.
Podemos ur al repo para ver esta libreria:

```bash
npm repo bcrypt
```

Vamos a instalarla:

```bash
npm i bcrypt
```

Ahora en el modelo vamos a crear un metodo estatico que haga una hash de un password, lo hacemos en models/Usuario.js:

## Usuario.js:

```javascript
// Cargamos bcrypt:
const bcrypt = require('bcrypt');

// Creamos el método estatico para el hash del password:
usuarioSchema.statics.hashPassword = function (passwordEnClaro) {
  return bcrypt.hash(passwordEnClaro, 7);
};
```

NOTA: Este método estatico debemos colocarlo antes de crear el modelo. Si no fallaria.

Debemos leer la documentación de bcrypt para saber como funciona, pero en general aqui vemos que le pasamos un password en plano y con un numero de vueltas. Este valor no puede ser muy grande ya que se relentiza al hacer el cifrado.

Ahora vamos a usar este método del modelo, nos vamos a init-db.js que es donde hacemos la insercion de esos usuarios:

## init-db.js:

```javascript
// Funcion que crear usuarios encriptados leyendo el arreglo de init-db-data.json:
async function initUsuarios() {
  try {
    // eliminar
    const deleted = await Usuario.deleteMany();
    console.log(`Eliminados ${deleted.deletedCount} usuarios.`);

    // Crear nuevos usuarios con contraseñas hasheadas
    const usuariosConHash = await Promise.all(
      initData.usuarios.map(async (usuario) => {
        const hashedPassword = await Usuario.hashPassword(usuario.password);
        return {
          email: usuario.email,
          password: hashedPassword,
        };
      })
    );

    const inserted = await Usuario.insertMany(usuariosConHash);
    console.log(`Creados ${inserted.length} usuarios.`);
  } catch (error) {
    console.error('Error:', error);
  }
}
```

Como vemos hemos modificado la funcion initUsuarios(), hemos iterado el json de usuario y hemos retornado un nuevo objeto con el password ya encriptado. Como la funcion bcrypt retorna una promesa debemos usar Promise.all() para cuando se resulva la promesa y obtengamos los valores pasarlos a la funcion que crea los usuarios e bd.

Lo probamos y vemos que funciona. Los revisamos y vemos que ya estan encriptados los usuarios con sus password encriptados. Ya hemos protegido nuestra información.

Ahora debemos modificar tamben donde hacemos login porque estamos comparando lo que nos viene con lo que tenemos en la bd, recibimos un texto plano y en la bd tenemos un hash encriptado. Esto lo vemos en el controlador LoginController.js

Para solucionar esto vamos a usar otro método de la libreria bcript, es el metodo bcript.compare().
Para esto en el modelos vamos a crear un metodo mas de instancia para hacer la comparacion:

## Usuario.js:

```javascript
// Creamos un método de instanacia que comprueba la password de una usuario:
usuarioSchema.methods.comparePassword = function (passwordEnClaro) {
  return bcrypt.compare(passwordEnClaro, this.password);
};
```

Ahora vamos a usarlo en el loginCoontroller.js:

## LoginController.js:

```javascript
// Si no lo encuentra o contraseña mal retorno -> error:
if (!usuario || !(await usuario.comparePassword(password))) {
  // Respondemos con una variable que tenemos en la vista y multiidioma:
  res.locals.error = req.__('Invalid credentials');

  // Enviamos ala vista el email:
  res.locals.email = email;

  // Renderizamos de nuevo la vista:
  res.render('login');
  return;
}
```

Tambien se ha cambiando para usar el boton de login que tenemos en header para redireccionar a la pantalla de login.

🎯 Commit: Clase 2: Autenticación, se encriptan contraseñas de los usuarios.

## Mostrar informacion de la BD en zona privada:

Ahora lo que haremos es que cuando el usuario entre a su zona privada se muestren sus datos, por ejemplo el email.

Para esto en la vista mostramos la variable email:

## private.ejs:

```html
<div class="col-12 col-lg-5">
  <h2 class="display-4 lh-1 mb-4"><%=__('Private zone')%></h2>
  <p>User: <%= email%></p>
</div>
```

Ahora en el controlador de la sesion tenemos que sacar ese email:

## PrivateController.js:

```javascript
/*
Creamos nuestros controladores con clases:
*/
const Usuario = require('../models/Usuario');
const createError = require('http-errors');
class PrivateController {
  async index(req, res, next) {
    try {
      // obtener el id del usuario de la sesion
      const usuarioId = req.session.usuarioLogueado;

      // buscar el usuario en la bd:
      const usuario = await Usuario.findById(usuarioId);

      if (!usuario) {
        next(createError(500, 'usaurio no encontrado'));
        return;
      }
      // Renderizamos la vista y la variable local
      res.render('private', { email: usuario.email });
    } catch (err) {
      next(err);
    }
  }
}

module.exports = PrivateController;
```

Como podemos ver hemos usado el modelo para acceder a la base de datos y por medio del metodo .findById() obtenemos el usuario, El id lo hemos sacada de la sesion. Luego en la vista vemos como hemos pasado un objeto con esa informacion que luego se renderizara en la vista como variable global. Tambien hemos metido nuestro codigo en un trycacht para controlar los errores y hemos usado la libreria http-error para manejar esos errores y usar el middleware de errores.

🎯 Commit: Clase 2: Autenticación, mostrar información del usuario logueado de la base de datos.
