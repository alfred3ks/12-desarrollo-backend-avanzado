# Clase 2: Como estructurar en nuestra app los controladores.

Ahora podemos ver la estructura actual del proyecto:

![](./img/estructura-actual.png)

Esto lo vamos a cambiar, los controladores que tenemos en la carpeta routes, existe una forma de estructurar mejor para el testing.

La manera como la tenemos estructurada la aplicacion no es facil preparar los test de testing.

Vamos a ver como hacerlo en el middleware de features.js.

En la raiz del proyecto vamos a crear una nueva carpeta llamada controllers.

nodeapp/controllers.

Creamos el controlador FeaturesController.js:

## FeaturesController.js:

```javascript
/*
Creamos nuestros controladores con clases:
*/
class FeaturesController {
  index(req, res, next) {
    res.render('features');
  }
}

module.exports = FeaturesController;
```

Luego para usarlo lo cargamos en app.js:

## app.js:

```javascript
// 📌 Cargamos los controladores:
const FeaturesController = require('./controllers/FeaturesController');
/**
 * Rutas del website
 */
// 📌 Creamos la ruta de esta manera:
// app.use('/features', require('./routes/features'));
app.get('/features', featuresController.index);
```

Lo probamos y vemos que funciona. Como podemos ver lo hemos hecho con clases, pero podemos crear un objeto y exportar ese objeto. Es igual la técnica que usemos. Con clases nos viene bien porque siempre queremos exportar varios métodos, para este caso solo tiene un middleware. No es el caso de por ejemplo index.js que lo vemos en routes/index.js que ya tiene varios middleware y tendremos que crear varios métodos.

Lo ideal para test es hacerlo asi. Ya el archivo routes/features.js lo podemos eliminar.

Ahora vamos a crear el controlador de routes/change-locale.js.

Para hacer más caracteristico este controlador la llamaremos LangController.js:

## LangController.js:

```javascript
class LangController {
  changeLocale(req, res, next) {
    const locale = req.params.locale;
    // poner una cookie con el nuevo idioma
    res.cookie('nodeapp-locale', locale, {
      maxAge: 1000 * 60 * 60 * 24 * 30, // 30 dias
    });

    // repondemos con una redireccion a la misma pagina de la que venia:
    // referer lo vemos que lo da la peticion cuando vamos de una pagina a otra lo vemos en las devtoools
    res.redirect(req.get('referer'));
  }
}

module.exports = LangController;
```

Ahora lo importamos en app.js:

## app.js:

```javascript
// 📌 Cargamos los controladores:
const LangController = require('./controllers/LangController');

/**
 * Rutas del website
 */
// app.use('/change-locale', require('./routes/change-locale'));
app.get('/change-locale/:locale', langController.changeLocale);
```

NOTA: He conservado los dos archivos en routes, para ver como eran y ahora como es el código pasado a controlador, los archivos los he renombrado asi:

\_features.js

\_change-locale.js

Ahora lo ideal y lo que se debe hacer es cambiar todos a controladores.
