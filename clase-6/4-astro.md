# Vamos a ver este otro framework para node.js:

https://astro.build/

Este esta pegando duro sobre todo para crear un website ricos en contenido, por ejemplo un blog. O un curriculum, o una tienda online con muchos productos o el website de la empresa donde trabajamos.etc.

Para una api no seria muy recomendado. Un dashboart tampoco es tan recomendado, usariamos mejor a express.

Astro tiene cierto parecido a express en ciertas cosas.

Vamos a crear un proyecto:

```bash
npm create astro@latest
```

Para este ejemplo usaremos typescript, y en la configuracion escogeremos que se Relaxed, cuando lo prengunte.

Para arrancar vemos en el package.json cual es el script:

```bash
npm run dev
```

Vemos que se despliega en:

http://localhost:4321/

Asi como arranca este website esta en modo SSG. ¿Que es SSG?.

SSG -> static site generation. Esto viene por defecto. Todas las paginas del web site no son dinamicas son estaticas.

Vamos a ver como funcional astro, en la carpeta pages nos creamos una pagina, contact.astro:

## contact.astro:

```javascript
---
import Layout from '../layouts/Layout.astro';
---

<Layout title='Contacto'>
  <a href="/">Ir al inicio</a>
  <p>Esta es la página de contacto</p>
</Layout>

<!-- Asi metemos estilos -->
<style>
  html {
    background-color: white;
  }
</style>
```

Y luego vamos a buscador y ponemos:

http://localhost:4321/contact

Vemos que ya tenemos esa página corriendo. Vemos que con muy poco podemos tener una página web.

## Tipos de renderizados.

Astro funciona y todo estos modos. Y los puede combinar en función de la necesidad.

En esta web podemos ver un poco de que va:

https://fajarwz.com/blog/web-rendering-what-is-ssr-csr-ssg-and-isr/

SSG -> static site generation. Esto viene por defecto. Todas las paginas del web site no son dinamicas son estaticas.

![](./img/ssg.png)

SSR -> server side rendering, es como lo hemos usando en nodepop, express lo usa, el servidor cuando recibe una peticion renderiza esa pagina, usando unos template.

![](./img/ssr.png)

CSR -> client side rendering, es lo que hace react, es el browser el que renderiza la página y la pinta.

![](./img/csr.png)

Ahora en astro para cambiarlo todo a SSR lo vemos en su documentación. Debemos tirar este comando:

```bash
npx astro add node
```

Esto lo que dice es que vamos a desplegar en un servidor que lanzara un proceso de node.

🎯 Commit: Clase 6: Astro.

## Añadir framework de frontend:

Astro nos facilita poder añadir framework de frontend. Por ejemplo tailwind. Vamos a añadirlo:

```bash
npx astro add tailwind
```

Ahora ya podemos usar tailwind en el proyecto. Al volver a arrancar vemos que ha cambiado un poco de aspecto la web.

Hagamos un cambio en el link de la pagina de contact:

## contact.astro:

```html
<!-- Asi metemos estilos -->
<style>
  html {
    background-color: white;
    margin: 0 20px;
  }
  a {
    color: crimson;
    text-decoration: underline;
  }
</style>
```

Tambien podemos agregar react a astro:

```bash
npx astro add react
```

Vamos a meter un componente de react, lo metemos en la carpeta components:

## Counter.js:

```javascript
import { useState } from 'react';

const Counter = () => {
  const [count, setCount] = useState(0);

  const increment = () => {
    setCount(count + 1);
  };

  const decrement = () => {
    setCount(count - 1);
  };
  const reset = () => {
    setCount(0);
  };

  return (
    <div>
      <h2>Contador: {count}</h2>
      <button onClick={increment}>Incrementar +</button>
      <button onClick={decrement}>Decrementar -</button>
      <button onClick={reset}>Reset</button>
    </div>
  );
};

export default Counter;
```

## contact.astro:

```javascript
---
import Counter from '../components/Counter';
import Layout from '../layouts/Layout.astro';
---

<Layout title='Contacto'>
  <a href="/">Ir al inicio</a>
  <p>Esta es la página de contacto</p>
  <Counter client:visible/>
</Layout>

<!-- Asi metemos estilos -->
<style>
  html {
    background-color: white;
    margin: 0 20px;
  }
  a{
    color: crimson;
    text-decoration: underline;
  }
</style>
```

Como vemos importamos el contado en la cabecera y para poder usar el contador debemos pasarle la propiedad client:visible, con esto se ejecutara en el browser.

🎯 Commit: Clase 6: Astro, añadir framework de frontend como tailwind y react.

## Meter una API:

Tambien podemos meter una API a este frontend, esto es mas de backend. Dentro de la carpeta pages nos creamos una carpeta llamada api. Dentro nos creamos otra carpeta llamada agentes y dentro un index.ts.

## index.ts:

```typescript
import type { APIRoute } from 'astro';

export const GET: APIRoute = ({ params, request }) => {
  return new Response(
    JSON.stringify({
      result: [
        {
          name: 'Smith',
          age: 45,
        },
        {
          name: 'Brown',
          age: 38,
        },
      ],
    })
  );
};
```

http://localhost:4321/api/agentes

Como vemos ya tenemos una api:

![](./img/api-astro.png)

🎯 Commit: Clase 6: Astro, como meter una API.
