# Como hacer upload de imagenes en la api:

Lo que haremos es que cuando se cree un agente se le meta un avatar. Para eso vamos a modificar el modelo del agente.

# Agente.js:

```javascript
const agenteSchema = mongoose.Schema(
  {
    name: { type: String, index: true },
    age: { type: Number, index: true, min: 18, max: 120 },
    owner: { ref: 'Usuario', type: mongoose.Schema.Types.ObjectId },
    avatar: String,
  },
  {
    // collection: 'agentes' // para forzar un nombre concreto de colección
  }
);
```

Lo que buscamos es que cuando hacemos una petición a la api, api/agentes.js cuando creamos el agente queremos que puedan subir un fichero, guardaremos en la bd la referencia a ese fichero t dentro de la app una carpeta upload.

Para poder recibir upload lo tenemos que revisar en app.js, ahora mismo como esta no se puede, para los upload se usa otra codificación.

Para esto vamos a usar una libreria.

```bash
npm repo multer
```

https://github.com/expressjs/multer

```bash
npm i multer
```

Vamos a hacer un módulo de configuración para lo upload. Lo meteremos en la carpeta lib:

## uploadConfigure.js:

```javascript
const multer = require('multer');

// declaro una configuracion de upload:
const upload = multer({
  dest: 'uploads/',
});

module.exports = upload;
```

Ahora nos vamos a las rutas donde esta la api/agentes.js y le agregamos el metodo post de crear agente ese middleware:

## agentes.js:

```javascript
// POST /api/agentes
// Crea un agente
router.post('/', upload.single('avatar'), async (req, res, next) => {
  try {
    const agenteData = req.body;
    // Filtramos usando el id del usuario: Filtramos por owner: Obenemos el id del usuario logueado:
    const usuarioLogueadoID = req.usuarioLogueadoAPI;

    // Aqui nos llega el fichero, gracias a multer:
    // console.log(req.file);

    // creamos una instancia de agente en memoria
    const agente = new Agente(agenteData);
    // Le asignamos al agente el id del usuario logueado:
    agente.owner = usuarioLogueadoID;

    // Tratamos el avatar:
    agente.avatar = req.file.filename; // Este es el nombre raro que nos viene.

    // la persistimos en la BD
    const agenteGuardado = await agente.save();

    res.json({ result: agenteGuardado });
  } catch (err) {
    next(err);
  }
});
```

Como podemos ver hemos añadido este middleware: Lleva por parametro una cadena del nombre que usaremos del input a enviar por postman:

```javascript
upload.single('avatar'),
```

Tambien vemos que hemos asignado cuando se crea un agente que se le asigne el id del usuario que esta logueado:

```javascript
// Filtramos usando el id del usuario: Filtramos por owner: Obenemos el id del usuario logueado:
const usuarioLogueadoID = req.usuarioLogueadoAPI;
// Le asignamos al agente el id del usuario logueado:
agente.owner = usuarioLogueadoID;
```

Tambien debemos crear en la raiz del proyecto la carpeta uploads. Esto lo cambia mas adelante y lo que hace es que el avatar lo guarde en public, y ahi crea una carpeta llamada avatares.

Vemos lo que nos trae al hacer un upload de la imagenes con postman:

```javascript
// Aqui nos llega el fichero, gracias a multer:
console.log(req.file);
```

![](./img/multer.png)

Descanso--

Seguimos...

Cuando hacemos un update con postman con una imagen se nos guarda un fichero dentro de la carpeta upload, pero que no tiene extension y aparte genera un nombre poco visible, es una cadena de numeros y letras. Lo vemos a continuación:

![](./img/upload.png)

Para solucionar esto vamos a modificar el archivo uploadConfigure.js, crearemos un objeto de configuracion donde usaremos el metodo diskStorage() donde le diremos la direccion de donde guardar la imagen y como sera nombrada, luego este objeto de confugracion se lo pasaremos a multer.

## uploadConfigure.js:

```javascript
const multer = require('multer');
const path = require('path');

// Creamos la configuracion de almacenamiento para tratar la imagen:
const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    // Le decimos donde se va a guardar:
    const ruta = path.join(__dirname, '..', 'public', 'avatares');
    callback(null, ruta);
  },
  filename: function (req, file, callback) {
    // Le colocamos un timestand para diferenciar imagenes iguales ${Date.now()}
    const fileName = `${file.fieldname}-${Date.now()}-${file.originalname}`;
    callback(null, fileName);
  },
});

// declaro una configuracion de upload:
const upload = multer({
  storage,
});

module.exports = upload;
```

Lo probamos con postman:

![](./img/postman.png)

Vemos que funciona correctamente.

Tambien si hacemos la peticion en el frontend vemos que nos muestra todos los agentes de ese usuario:

![](./img/frontend.png)

NOTA: Lo recomendado es el proyecto ignorar en el .gitignore la carpeta avatares.

🎯 Commit: Clase 6: Upload de images por la api.
