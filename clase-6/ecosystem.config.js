module.exports = {
  apps: [
    {
      name: 'nodeapp',
      script: './nodeapp/bin/www',
      watch: '.',
      env_production: {
        NODE_ENV: 'production',
      },
      env_development: {
        NODE_ENV: 'development',
      },
      log_date_format: 'YYYY-MM-DD HH:mm',
    },
    {
      name: 'Micro servicio de email',
      script: './micro-services/emailSenderCote.js',
      watch: ['./micro-services/emailSenderCote.js'],
      instances: 5,
    },
  ],

  deploy: {
    production: {
      user: 'alfredo',
      host: 'nodeapp.com',
      ref: 'origin/main',
      repo: 'https://gitlab.com/alfred3ks/12-desarrollo-backend-avanzado',
      path: '/home/nodeapp/app',
      'pre-deploy-local': '',
      'post-deploy':
        'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': '',
    },
  },
};
