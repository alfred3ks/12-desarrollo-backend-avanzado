# Implementar microservicios en nodeapp:

En el módelo usuario vamos a crear otro método para usar microservicios con la libreria cote:

Tenemos que instalar la libreria cote:

```bash
npm i cote
```

## Usuario.js:

```javascript
// Cargamos la libreria cote:
const { Requester } = require('cote');
const request = new Requester({ name: 'nodeapp-email' });

// Método usando microservicios y libreria cote:
usuarioSchema.methods.sendEmailCote = async function (asunto, cuerpo) {
  // Creamos un evento:
  const evento = {
    type: 'enviar-email',
    from: process.env.EMAIL_SERVICE_FROM,
    to: this.email,
    subject: asunto,
    html: cuerpo,
  };

  return new Promise((resolve) => request.send(evento, resolve));
};
```

Ahora en el controlador que es donde usaremos este método, LoginController.js:

## LoginController.js

```javascript
// Llamamos el método para mandar email usando la libreria cote:
const result = await usuario.sendEmailCote(
  'Bienvenido',
  'Bienvenido a NodeApp'
);
// Vemos lo que nos devuelve cote:
console.log(result);
```

Ya tenemos en nodeapp la parte que se encarga de pedir que alguien mande ese email. Ahora vamos a crearnos un fichero que sera quien mande ese email. Nos creamos una carpeta llamada micro-services. Al igual que hicimos en la clase 5. Le creamos su package.json, las variables de entorno .env y le instalamos las librerias cote, dotenv, nodemailer

```bash
npm init -y
```

```bash
npm i cote dotenv nodemailer
```

Creamos nuestro archivo emailSenderCote.js:

## emailSenderCote.js:

```javascript
'use-strict';

const { Responder } = require('cote');
const nodemailer = require('nodemailer');

main().catch((err) => {
  console.log('Hubo un error', err);
});

async function main() {
  try {
    // Creamos el transporter:
    const transport = await createTransport();

    const responder = new Responder({ name: 'servicio de email' });
    responder.on('enviar-email', async (req, done) => {
      try {
        const { from, to, subject, html } = req;
        const result = await transport.sendMail({ from, to, subject, html });
        console.log(
          `Email enviando. URL: ${nodemailer.getTestMessageUrl(result)}`
        );
        done(result);
      } catch (error) {
        done({ message: error.message });
      }
    });
  } catch (error) {
    done({ message: error.message });
  }
}

// Funcion para crear un transporter:
async function createTransport() {
  // Entorno de desarrollo:
  // Creamos una cuenta de prueba:
  const testAccount = await nodemailer.createTestAccount();

  const developmentTransport = {
    host: testAccount.smtp.host,
    port: testAccount.smtp.port,
    secure: testAccount.smtp.secure,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  };

  return nodemailer.createTransport(developmentTransport);
}
```

Arrancamos la aplicación y lo probamos:

```bash
npm run email-sender-cote
```

Ahora hacemos login en nuestra nodeapp y vemos en la consola donde arrancamos email sender cote que nos da la url para ver el correo:

https://ethereal.email/message/ZZ.xL71VE-MlJM.mZZ.xiPruXtLvoOJyAAAAAU4zK.gDBc4HjNXEj1yCwy4

Comprobamos que funciona correactamente.

![](./img/cote.png)

🎯 Commit: Clase 6: Microservicios, libreria cote implementada en nodeapp.
