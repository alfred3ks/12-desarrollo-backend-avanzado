import type { APIRoute } from "astro"

export const GET: APIRoute = ({ params, request }) => {
  return new Response(JSON.stringify({
    result: [
      {
        name: 'Smith',
        age: 45
      },
      {
        name: 'Brown',
        age: 38
      }
    ]
  }
  ))
}