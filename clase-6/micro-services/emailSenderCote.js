'use-strict';

const { Responder } = require('cote');
const nodemailer = require('nodemailer');

main().catch((err) => {
  console.log('Hubo un error', err);
});

async function main() {
  try {
    // Creamos el transporter:
    const transport = await createTransport();

    const responder = new Responder({ name: 'servicio de email' });
    responder.on('enviar-email', async (req, done) => {
      try {
        const { from, to, subject, html } = req;
        const result = await transport.sendMail({ from, to, subject, html });
        console.log(
          `Email enviando. URL: ${nodemailer.getTestMessageUrl(result)}`
        );
        done(result);
      } catch (error) {
        done({ message: error.message });
      }
    });
  } catch (error) {
    done({ message: error.message });
  }
}

// Funcion para crear un transporter:
async function createTransport() {
  // Entorno de desarrollo:
  // Creamos una cuenta de prueba:
  const testAccount = await nodemailer.createTestAccount();

  const developmentTransport = {
    host: testAccount.smtp.host,
    port: testAccount.smtp.port,
    secure: testAccount.smtp.secure,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  };

  return nodemailer.createTransport(developmentTransport);
}
