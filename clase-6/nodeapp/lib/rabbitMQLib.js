'use-strict';
// Cargamos la libreria amqplib:
const amqplib = require('amqplib');

// Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
const canalPromise = amqplib
  .connect(process.env.RABBITMQ_BROKER_URL)
  .then((connection) => {
    // Crear un canal:
    return connection.createChannel();
  });

module.exports = canalPromise;
