const multer = require('multer');
const path = require('path');

// Creamos la configuracion de almacenamiento para tratar la imagen:
const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    // Le decimos donde se va a guardar:
    const ruta = path.join(__dirname, '..', 'public', 'avatares');
    callback(null, ruta);
  },
  filename: function (req, file, callback) {
    // Le colocamos un timestand para diferenciar imagenes iguales ${Date.now()}
    const fileName = `${file.fieldname}-${Date.now()}-${file.originalname}`;
    callback(null, fileName);
  },
});

// declaro una configuracion de upload:
const upload = multer({
  storage,
});

module.exports = upload;
