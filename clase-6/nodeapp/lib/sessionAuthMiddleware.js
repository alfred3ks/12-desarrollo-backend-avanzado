// modulo que esporta un middleware que controla si estamos logueados o no.

module.exports = (req, res, next) => {
  // si el cliente que hace la peticion no tiene en su sesion la variable usuarioLogueado le mandamos al login.
  if (!req.session.usuarioLogueado) {
    res.redirect('/login');
    return;
  }
  next();
};
