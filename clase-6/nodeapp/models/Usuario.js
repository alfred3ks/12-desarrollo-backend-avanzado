const mongoose = require('mongoose');
const nodemailer = require('nodemailer');

// Cargamos bcrypt:
const bcrypt = require('bcrypt');

// Cargamos el transport para enviar email:
const emailTransportConfigure = require('../lib/emailTransportConfigure');

// Cargamos el módulo de rabbitRQ:
const canalPromise = require('../lib/rabbitMQLib');

// Cargamos la libreria cote:
const { Requester } = require('cote');
const request = new Requester({ name: 'nodeapp-email' });

// creamos esquema:
const usuarioSchema = mongoose.Schema({
  email: { type: String, unique: true },
  password: String,
});

// Creamos el método estatico para el hash del password:
usuarioSchema.statics.hashPassword = function (passwordEnClaro) {
  return bcrypt.hash(passwordEnClaro, 7);
};

// Creamos un método de instanacia que comprueba la password de una usuario:
usuarioSchema.methods.comparePassword = function (passwordEnClaro) {
  return bcrypt.compare(passwordEnClaro, this.password);
};

// Método para enviar correos al usuario:
usuarioSchema.methods.sendEmail = async function (asunto, cuerpo) {
  // crear un transport:
  const transport = await emailTransportConfigure();

  // enviar email:
  const result = await transport.sendMail({
    from: process.env.EMAIL_SERVICE_FROM,
    to: this.email,
    subject: asunto,
    // text: -> email para texto plano,
    html: cuerpo,
  });
  console.log(
    `URL de previsualización: ${nodemailer.getTestMessageUrl(result)}`
  );
  return result;
};

// Método para enviar correo usando microservicios: Otro servicio (rabbitMQ) envia el email.
usuarioSchema.methods.sendEmailRabbitMQ = async function (asunto, cuerpo) {
  // Cargar módulo rabbitMQLib y enviamos un mensaje:
  const canal = await canalPromise;

  // asegurar que existe el exchange:
  const exchange = 'email-request';
  await canal.assertExchange(exchange, 'direct', {
    durable: true, //the exchange will survive broker restarts.
  });

  // publicar mensaje:
  const mensaje = {
    asunto,
    to: this.email,
    cuerpo,
  };

  canal.publish(exchange, '*', Buffer.from(JSON.stringify(mensaje)), {
    persistent: true, // the message will survive broker restarts
  });
};

// Método usando microservicios y libreria cote:
usuarioSchema.methods.sendEmailCote = async function (asunto, cuerpo) {
  // Creamos un evento:
  const evento = {
    type: 'enviar-email',
    from: process.env.EMAIL_SERVICE_FROM,
    to: this.email,
    subject: asunto,
    html: cuerpo,
  };

  return new Promise((resolve) => request.send(evento, resolve));
};

// creamos el modelo:
const Usuario = mongoose.model('Usuario', usuarioSchema);

// exportamos el modelo
module.exports = Usuario;
