/*
Creamos nuestros controladores con clases:
*/
// Cargamos los modelo de Agentes y Usuario :
const { Usuario, Agente } = require('../models');
const createError = require('http-errors');

class PrivateController {
  async index(req, res, next) {
    try {
      // obtener el id del usuario de la sesion
      const usuarioId = req.session.usuarioLogueado;

      // buscar el usuario en la bd:
      const usuario = await Usuario.findById(usuarioId);

      if (!usuario) {
        next(createError(500, 'usuario no encontrado'));
        return;
      }

      // Obtenemos la lista de agentes de la bd que pertencen al usuario:
      const agentes = await Agente.find({ owner: usuarioId });

      // Renderizamos la vista y la variable local
      res.render('private', { email: usuario.email, agentes });
    } catch (err) {
      next(err);
    }
  }
}

module.exports = PrivateController;
