'use strict';

// Cargamos la libreria dotenv para variables de entorno:
require('dotenv').config();

const readline = require('node:readline');
const connection = require('./lib/connectMongoose');
const initData = require('./init-db-data.json');

// Cargamos los modelos:
// const Agente = require('./models/Agente');
// const Usuario = require('./models/Usuario');

// Asi cargamos todos los modelos en un solo require:
const { Agente, Usuario } = require('./models');

main().catch((err) => console.log('Hubo un error', err));

async function main() {
  // espero a que se conecte a la base de datos
  await new Promise((resolve) => connection.once('open', resolve));

  const borrar = await pregunta(
    'Estas seguro de que quieres borrar la base de datos y cargar datos iniciales? '
  );
  if (!borrar) {
    process.exit();
  }

  // inicializar la colección de usuarios
  await initUsuarios();

  // inicializar la colección de agentes
  await initAgentes();

  connection.close();
}

// Funcion que crea agentes de inicio en la BD:
async function initAgentes() {
  // borrar todos los documentos de la colección de agentes
  const deleted = await Agente.deleteMany();
  console.log(`Eliminados ${deleted.deletedCount} agentes.`);

  // Obtenemos el id del usuario de bd:
  const [adminUser, usuario1] = await Promise.all([
    Usuario.findOne({ email: 'admin@example.com' }),
    Usuario.findOne({ email: 'usuario1@example.com' }),
  ]);

  // Le asignamos los usuarios a los agentes:
  const dataAgentes = initData.agentes.map((agente, index) => {
    if (index === 0) {
      return { ...agente, owner: adminUser._id };
    } else if (index === 1) {
      return { ...agente, owner: adminUser._id };
    } else if (index === 2) {
      return { ...agente, owner: usuario1._id };
    }
    return agente;
  });

  // crear agentes iniciales:
  const inserted = await Agente.insertMany(dataAgentes);
  console.log(`Creados ${inserted.length} agentes.`);
}

// Funcion que crea usuarios en la BD: solucion profesor sin encriptar:
// async function initUsuarios() {
//   // eliminar
//   const deleted = await Usuario.deleteMany();
//   console.log(`Eliminados ${deleted.length} usuarios.`);

//   // crear
//   const inserted = await Usuario.insertMany(initData.usuarios);
//   console.log(`Creados ${inserted.length} usuarios.`);
// }

// Funcion que crea usuarios en la BD: solucion profesor encriptados:
// async function initUsuarios() {
//   // eliminar
//   const deleted = await Usuario.deleteMany();
//   console.log(`Eliminados ${deleted.length} usuarios.`);

//   // crear
//   const inserted = await Usuario.insertMany([
//     {
//       email: 'admin@example.com',
//       password: await Usuario.hashPassword('1234'),
//     },
//     {
//       email: 'usuario1@example.com',
//       password: await Usuario.hashPassword('1234'),
//     },
//   ]);
//   console.log(`Creados ${inserted.length} usuarios.`);
// }

// Funcion que crear usuarios encriptados leyendo el arreglo de init-db-data.json:
async function initUsuarios() {
  try {
    // eliminar
    const deleted = await Usuario.deleteMany();
    console.log(`Eliminados ${deleted.deletedCount} usuarios.`);

    // Crear nuevos usuarios con contraseñas hasheadas
    const usuariosConHash = await Promise.all(
      initData.usuarios.map(async (usuario) => {
        const hashedPassword = await Usuario.hashPassword(usuario.password);
        return {
          email: usuario.email,
          password: hashedPassword,
        };
      })
    );

    const inserted = await Usuario.insertMany(usuariosConHash);
    console.log(`Creados ${inserted.length} usuarios.`);
  } catch (error) {
    console.error('Error:', error);
  }
}

function pregunta(texto) {
  return new Promise((resolve, reject) => {
    // conectar readline con la consola
    const ifc = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    ifc.question(texto, (respuesta) => {
      ifc.close();
      resolve(respuesta.toLowerCase() === 'si');
    });
  });
}
