# Clase 4: Implementar JWT segunda parte:

Vamos a ver que nuestra API no tiene autenticación ahora mismo, asi que nos vamos a app.js, rutas de la api: si hacemos la busqueda por url http://localhost:3000/api/agentes vemos que la ruta esta abierta calquiera podria ver la informacion lo que haremos proteger con autenticacion las rutas de la api. Como vemos ya en fundamentos si habiamos colocado una proteccion basica pero la de ahora la haremos obteniendo el JWT si esa persona esta logueada antes. Para esto vamos a crearnos un middleware.

## app.js:

```javascript
/**
 * Rutas del API
 */
app.use('/api-doc', swaggerMiddleware);

// middleware de JWT:
app.post('/api/login', loginController.postJWT);
app.use('/api/agentes', require('./routes/api/agentes'));
```

Como vemos la ruta /api/agentes no tiene ninguna proteccion. Para eso en la carpeta lib nos crearemos el middleware de autenticacion para jwt.

NOTA: Aqui el secret que tenemos cuando creamos en loginController.js e token lo hemos llevado a variable de entorno ya que es un valor que vamos a usar tanto en ese middleware como en el middleware de autenticacion por token.

## .env:

```bash
MONGODB_URI=mongodb://127.0.0.1/cursonode
JWT_SECRET=s876ads87dasuytasduytasd
```

## LoginController.js:

```javascript
// si existe y la constraseña coincide --> devuelvo un JWT:
// el metodo sing recibe tres parametros, 1 un {} con lo deseamos enviar, 2 la clave secreta, es un valor aleatoro puesto por nosotros, 3 un objeto de opciones con la expiracion del token:
const tokenJWT = await jwt.sign({ _id: usuario._id }, process.env.JWT_SECRET, {
  expiresIn: '2h',
});
```

## jwtAuthMiddleware.js:

```javascript
const jwt = require('jsonwebtoken');
const createError = require('http-errors');

// modulo que exporta un middleware:

module.exports = async (req, res, next) => {
  try {
    // recoger el jwtToken de la cabecera o del body o de la query string:
    const jwtToken = req.get('Authorization') || req.body.jwt || req.query.jwt;

    // comprobar que mandado un jwtToken:
    if (!jwtToken) {
      next(createError(401, 'no token provided'));
      return;
    }

    // comprobaremos que el token es valido: Usamos la libreria jsonwebtoken:
    jwt.verify(jwtToken, process.env.JWT_SECRET, (err, payload) => {
      if (err) {
        next(createError(401, 'invalid token'));
        return;
      }

      // Apuntamos el usuario loguedo en la request: Asi ya tendremos esa informacion del usuario disponible para cualquier otro middleware: Lo vemos en el middleware api/agentes.js
      req.usuarioLogueadoAPI = payload._id;

      // dejamos pasar al siguiente middleware:
      next();
    });
  } catch (err) {
    next(err);
  }
};
```

Ahora metemos el middleware en la ruta para proteger la ruta, eso lo hacemos en app.js:

## app.js:

```javascript
// Agrupamos los middleware:
const jwtAuthMiddleware = require('./lib/jwtAuthMiddleware');

/**
 * Rutas del API
 */
app.use('/api/agentes', jwtAuthMiddleware, require('./routes/api/agentes'));
```

Como podemos ver hemos colocado en el objeto req el id del usuario que obtenemos del payload del JWT, esto para tenerlo disponible en los siguientes middleware y no tenener que buscarlos cada vez:

```javascript
// Apuntamos el usuario loguedo en la request: Asi ya tendremos esa informacion del usuario disponible para cualquier otro middleware: Lo vemos en el middleware api/agentes.js
req.usuarioLogueadoAPI = payload._id;
```

Lo podemos comprobar en el middleware de la api/agentes cuando hacemos la peticion y mostrarlo por consola y comprobar que tenemos ese dato disponible:

## api/agentes.js:

```javascript
router.get('/', async (req, res, next) => {
  try {
    // escribimos en el log el id del usuario loguedo en el API con JWT:
    console.log('Usuario API id: ', req.usuarioLogueadoAPI);
```

Ahora vamos a ver si esta funcionando lo que hemos realizado:

Lo probamos con postman: https://localhost:3000/api/agentes por medio de una peticion get:

![](./img/token.png)

Funciona... La ruta esta protegida si no hay un token ahora vamos a probar mandarle un token por la cabecera:

![](./img/token2.png)

Ahora vemos que si funciona ya nos deja pasar. Tambien vemos como es cierto que ya tenemos el id del usuario del token en el objeto req:

![](./img/token3.png)

Vamos a modificar ahora el middleware de agentes y usando es id que ya tenemos en req, mostrar contenido filtrado:

## api/agentes.js:

```javascript
// Filtramos usando el id del usuario: Filtramos por owner:
const usuarioLogueadoID = req.usuarioLogueadoAPI;
filtro.owner = usuarioLogueadoID;
```

Con esto ahora solo nos mostrara los agentes que son de este usuario que este logueado, antes mostraba todos los agentes que existian.

![](./img/token4.png)

Ahora ya tenemos que la api muestra los recursos que son propiedad del usuario de que hace la peticion.

🎯 Commit: Clase 4: Implementación de JWT segunda parte.
