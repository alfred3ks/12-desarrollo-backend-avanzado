## Solución de lentitud al hacer login y ejemplo rabbit-mq:

Como podemos ver al hacer login estamos teniendo un poco de lentitud al hacer login, esto es porque estamos enviando un correo al usuario y lo estamos haciendo usando asincronismo, hasta que no se resuelve el envio no redirije a la pagina. Eso lo vemos en el LoginController.js:

## LoginController.js:

```javascript
// mandamos un email al usuario: Recibe 2 parametros, asunto y el cuerpo del correo:
const emailResult = await usuario.sendEmail(
  'Bienvenido',
  'Bienvenido a NodeApp'
);
```

Para este caso concreto podemos quitar el await, ojo solo este caso concreto:

```javascript
// mandamos un email al usuario: Recibe 2 parametros, asunto y el cuerpo del correo:
usuario.sendEmail('Bienvenido', 'Bienvenido a NodeApp');
```

Esto lo podemos hacer porque como tal aqui no es necesario hacerlo. En otros casos que tengamos que enviar varios email, o que sea necesario usar para algo el resultado de la petición asincrona tedremos que usar otra tecnica para que el usuario no tenga que esperar.

Esto lo vemos en el pdf de la clase:

• Las respuestas a las peticiones HTTP deben ser casi inmediatas.
• Un backend lento, es un backend mal hecho.
• Pero a veces hay que hacer cosas que son lentas como enviar correo, redimensionar una imagen, leer archivos gigantes…
• Todas esas tareas lentas, deberemos hacerlas de manera diferida.

Una primera opción seria utilizar un cron. 1:34:25

Lo que hacemos es apuntar en la BD la tarea de enviar el correo y con cun cron que es un script que cada cierto tiempo que le asignemos nosotros se ejecutara y revisara si hay alguna tarea pendiente en BD por ejecutar.

Esta opcion es una pero no la mas recomendable.

Otra opción seria, usando un gestor de tareas. Existen varios gestores de tareas, tenemos RabbitMQ/ZeroMQ/Mongo/Redis.

Uno de los mas usados es RabbitMQ.

Esto es un software de cola de mensajes que implementa el protocolo AMQP. Aqui tenemso su web:

https://www.rabbitmq.com/

Es un envio de mensajes entre aplicativos o aplicaciones.

Para usar rabbitmq vamos a darnos de alta en un servicio externo:
Con el plan free nos he suficiente.

https://www.cloudamqp.com/

Creamos una cuenta, nos pide un correo. Al darnos de alta nos manda aqui: Ver usuario en claves.

![](./img/cola-tareas.png)

Le damos a crear una nueva instancia:

![](./img/cola-tareas1.png)

Luego le damos a seleccionar region:

![](./img/cola-tareas2.png)

Le damos luego a review:

![](./img/cola-tareas3.png)

Y le damos a crear instancia.

![](./img/cola-tareas4.png)

Ya esta ya tenemos creada nuestra instancia, el servicio. Lo siguiente sera crear un worker que se conectara a la aplicacion y este al servicio que acabamos de dar de alta.

Aqui hace descanso... 1:43:31.

Para ver las credenciales le damos donde dice cursonode y ahi vemos que se ven las credenciales para podernos conectar al servicio.

![](./img/credenciales.png)
![](./img/credenciales1.png)

Vamos a crearnos un ejemplo de como conectarnos. Lo he puesto dentro de la carpeta ejemplos y lo llamamos ejemplo-rabbit-mq.

Creamos dos archivos consumer.js y publisher.js y le creamos su package.json:

```bash
npm init -y
```

Vamos a usar una libreria llamada amqplib. amqp es el protocolo que usa rabbit-mq.

```bash
npm repo amqplib
```

https://github.com/amqp-node/amqplib

La instalamos:

```bash
npm i amqplib
```

## publish.js:

```javascript
'use-strict';

// Cargamos la libreria amqplib:
const amqplib = require('amqplib');
const EXCHANGE = 'task-request';

// Creamos una funcion main() y aqui la invocamos, como es asincrona lo hacemos asi:
main().catch((err) => console.log('Hubo un error: ', err));

async function main() {
  // Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
  const connection = await amqplib.connect(
    'amqps://niidpjaa:qNsmSD5PmlyDjtJpozkfAVvUbtGOi5Q6@whale.rmq.cloudamqp.com/niidpjaa'
  );

  // Crear un canal:
  const canal = await connection.createChannel();

  // asegurar que existe un exchange:
  await canal.assertExchange(EXCHANGE, 'direct', {
    durable: true, //the exchange will survive broker restarts.
  });

  // publicar mensaje:
  const mensaje = {
    tarea: 'enviar un email',
  };

  canal.publish(EXCHANGE, '*', Buffer.from(JSON.stringify(mensaje)), {
    persistent: true, // the message will survive broker restarts
  });

  console.log('Enviado mensaje', mensaje);
}
```

Lo ejecutamos a ver que funcione:

```bash
npx nodemon publish.js
```

Ahora nos vamos a servicio, rabbitMQ vemos en los exchange:

![](./img/rabbitmq.png)
![](./img/rabbitmq1.png)

Vemos el pico que se ha enviado un mensaje por medio de ese exchange. De momento no esta conectado en ninguna cola. Eso lo haremos en el lado del receptor, el que recibe el mensaje. Que es el consumer.js. Ahit tendremos una cola de recepción.

NOTA: Lo que significa el \* al publicar el mensaje:

```javascript
canal.publish(EXCHANGE, '*', Buffer.from(JSON.stringify(mensaje)), {
  persistent: true, // the message will survive broker restarts
});
```

Existen varios patrones que podemos usar, lo vemos en la documentación: El \* es a todas las colas:

https://rabbitmq.com/getstarted.html

![](./img/rabbitmq2.png)

![](./img/rabbitmq3.png)

Como podemos ver en la imagen anterior tenemos un publisher una cola de tarea y dos consumidores. Uno de ellos consumira el mensaje y luego la cola de tarea olvidares este.

Nuestro patron que usamos ahora en este ejemplo consiste en que el exchange lo publicara a todas las colas y en cada cola abra un consumidor. Este es mas un patron publish/subscribe.

Ahora lo que hacemos es meter en un bucle while() infinito en el envio del mensaje:

## publish.js:

```javascript
while (true) {
  // publicar mensaje:
  const mensaje = {
    tarea: 'enviar un email' + Date.now(),
  };
  canal.publish(EXCHANGE, '*', Buffer.from(JSON.stringify(mensaje)), {
    persistent: true, // the message will survive broker restarts
  });
  console.log('Enviado mensaje', mensaje);
}
```

Lo que hace es crear una funcion sleep() que envie cada cierto tiempo ese mensaje asi el bucle no va a lo loco.

## publish.js:

```javascript
// Funcion sleep:
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

while (true) {
  // publicar mensaje:
  const mensaje = {
    tarea: 'enviar un email' + Date.now(),
  };
  canal.publish(EXCHANGE, '*', Buffer.from(JSON.stringify(mensaje)), {
    persistent: true, // the message will survive broker restarts
  });
  console.log('Enviado mensaje', mensaje);

  // Usamos la funcion sleep:
  await sleep(3000);
}
```

Ahora vemos en el servicio como tenemos actividad de envio de mensajes cada 3 segundos:

![](./img/rabbitmq4.png)

Ahora vamos a crear una cola, donde enganchar esos mensajes.

## Creación del consumidor:

Instalamos dotenv para poder usar variables de entorno:

```bash
npm i dotenv
```

Y creamos nuestro .env:

## .env:

```
RABBITMQ_BROKER_URL=amqps://niidpjaa:qNsmSD5PmlyDjtJpozkfAVvUbtGOi5Q6@whale.rmq.cloudamqp.com/niidpjaa
```

## consumer.js:

```javascript
'use-strict';

// Cargamos la libreria amqplib:
const amqplib = require('amqplib');

//dotenv:
require('dotenv').config();

// definimos la cola:
const QUEUE = 'tasks';

// Funcion sleep:
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

// Creamos una funcion main() y aqui la invocamos, como es asincrona lo hacemos asi:
main().catch((err) => console.log('Hubo un error: ', err));

async function main() {
  // Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
  const connection = await amqplib.connect(process.env.RABBITMQ_BROKER_URL);

  // Crear un canal:
  const canal = await connection.createChannel();

  // asegurarnos que existe la cola donde vamos a recibir los mensajes:
  await canal.assertQueue(QUEUE, {
    durable: true, // the queue will surviver broken restarts
  });

  // Ahora consumimos los mensajes:
  canal.consume(QUEUE, (mensaje) => {
    console.log(mensaje);
  });
}
```

Y lo arrancamos por consola:

```bash
npx nodemon consumer.js
```

Ahora debemos hacer la conexion del exchange con la cola, se puede hacer por código pero el dice que no lo recomienda, lo haremos por una configuracion manual. Lo hacemos en rabbitrq:

![](./img//rabbitmq5.png)

Le damos a bind.

Ahora vemos que en la consola donde hemos arrancado a consumer.js ya empiezan a pasar cositas, se muestran mensajes. Y ahora vamos a las colas de la web y vemos que ya tenemos ahi una tarea:

![](./img//rabbitmq6.png)

Con la configuracion que tenemos en el publisher.js y en el consumer.js actualemente que vemos a continuacion:

## publisher.js:

```javascript
'use-strict';

// Cargamos la libreria amqplib:
const amqplib = require('amqplib');
const EXCHANGE = 'task-request';

//dotenv:
require('dotenv').config();

// Funcion sleep:
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

// Creamos una funcion main() y aqui la invocamos, como es asincrona lo hacemos asi:
main().catch((err) => console.log('Hubo un error: ', err));

async function main() {
  // Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
  const connection = await amqplib.connect(process.env.RABBITMQ_BROKER_URL);

  // Crear un canal:
  const canal = await connection.createChannel();

  // asegurar que existe un exchange:
  await canal.assertExchange(EXCHANGE, 'direct', {
    durable: true, //the exchange will survive broker restarts.
  });

  while (true) {
    // publicar mensaje:
    const mensaje = {
      tarea: 'enviar un email ' + Date.now(),
    };
    canal.publish(EXCHANGE, '*', Buffer.from(JSON.stringify(mensaje)), {
      persistent: true, // the message will survive broker restarts
    });
    console.log('Enviado mensaje', mensaje);

    // Usamos la funcion sleep:
    await sleep(100);
  }
}
```

## consumer.js:

```javascript
'use-strict';

// Cargamos la libreria amqplib:
const amqplib = require('amqplib');

//dotenv:
require('dotenv').config();

// definimos la cola:
const QUEUE = 'tasks';

// Creamos una funcion main() y aqui la invocamos, como es asincrona lo hacemos asi:
main().catch((err) => console.log('Hubo un error: ', err));
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

async function main() {
  // Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
  const connection = await amqplib.connect(process.env.RABBITMQ_BROKER_URL);

  // Crear un canal:
  const canal = await connection.createChannel();

  // asegurarnos que existe la cola donde vamos a recibir los mensajes:
  await canal.assertQueue(QUEUE, {
    durable: true, // the queue will surviver broken restarts
  });

  // le decimos a la cola que esta trabajando para evitar saturacion: Lo hara de uno en uno. Dame un mensaje y hasta que no lo procese no me des mas. Recibe por parametros el numero de msj:
  canal.prefetch(1); // pending ack

  // Ahora consumimos los mensajes:
  canal.consume(QUEUE, async (mensaje) => {
    const payload = mensaje.content.toString();
    console.log(payload);
    await sleep(150);
    canal.ack(mensaje);
  });
}
```

Y vemos que se estan encolando los mensajes eso lo vemos en la web el valor de ready:

![](./img/rabbitmq7.png)

Lo que tendriamos que hacer es arrancar mas consumidores. Eso lo hacemos con abrir el terminal y arrancar otra consumer llamando de nuevo el archivo consumer.js:

```bash
npx nodemon consumer.js
```

Ahora vemos como se igualan los consumer con los publish:

![](./img/rabbitmq8.png)

Ahora tendriamos dos consumidores. Esto lo hacemos sin tocar mas el código solo arrancando mas consumidores. Asi podremos escalar mucho mas facil nuestra app simplemente arrancando mas consumidores.

Ahora si deseamos que nuestro publicador se limite y solo publique cuando el buffer este libre, simplemente le añadimos esto:

## publisher.js:

```javascript
// Variable para constrolar la publicacion para evitar saturacion: Si es true puede seguir enviando si pasa a false es porque se ha saturado.
let keepSending = true;

// Verificamos si podemos enviar mas o tengo que esperar: Asi contenemos la publicacion:
if (!keepSending) {
  // Nos suscribimos a un evento: Espera a que se drene (vacie) el buffer de escritura del broker:
  console.log('Buffer lleno, espero a que se vacie');
  await new Promise((resolve) => canal.on('drain', resolve));
}

keepSending = canal.publish(
  EXCHANGE,
  '*',
  Buffer.from(JSON.stringify(mensaje)),
  {
    persistent: true, // the message will survive broker restarts
  }
);
```

🎯 Commit: Clase 4: Evitar relentización del login y ejemplo rabbit-mq.
