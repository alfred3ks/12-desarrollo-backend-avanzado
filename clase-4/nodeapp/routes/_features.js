// Este archivo fue pasado a controlador, lo vemos dentro de la carpeta controllers:
const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
  res.render('features');
});

module.exports = router;
