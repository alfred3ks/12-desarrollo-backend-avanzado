// Cargamos la libreria:
const nodemailer = require('nodemailer');

module.exports = async function () {
  // Entorno de desarrollo:
  // Creamos una cuenta de prueba:
  const testAccount = await nodemailer.createTestAccount();

  const developmentTransport = {
    host: testAccount.smtp.host,
    port: testAccount.smtp.port,
    secure: testAccount.smtp.secure,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  };

  // Entorno de producion: Vamos a usar un servicio externo:
  const productionTransport = {
    service: process.env.EMAIL_SERVICE_NAME,
    auth: {
      user: process.env.EMAIL_SERVICE_USER,
      pass: process.env.EMAIL_SERVICE_PASS,
    },
  };

  // Usamos la configuración del entorno en que se ejecuta la aplicación:
  const activeTransport =
    process.env.NODE_ENV === 'development'
      ? developmentTransport
      : productionTransport;

  const transport = nodemailer.createTransport(activeTransport);

  return transport;
};
