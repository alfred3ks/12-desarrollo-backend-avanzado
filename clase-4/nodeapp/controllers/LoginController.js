/*
Controlador para la página de login:
*/

// Cargamos el modelo:
const Usuario = require('../models/Usuario');

// Importamos la libreria JWT:
const jwt = require('jsonwebtoken');

class LoginController {
  // Método que carga el formulario:
  index(req, res, next) {
    // Inicializamos la variable local de error y email a vacio para que no pete la app:
    res.locals.error = '';
    res.locals.email = '';
    res.render('login');
  }

  // Método que comprueba el usuario al enviar el formulario:
  async post(req, res, next) {
    // console.log(req.body)
    try {
      // Extraemos los datos del body:
      const { email, password } = req.body;

      // Comprobamos que exista en bd el usuario:
      const usuario = await Usuario.findOne({
        email: email,
      });

      // Si no lo encuentra o contraseña mal retorno -> error:
      if (!usuario || !(await usuario.comparePassword(password))) {
        // Respondemos con una variable que tenemos en la vista y multiidioma:
        res.locals.error = req.__('Invalid credentials');

        // Enviamos ala vista el email:
        res.locals.email = email;

        // Renderizamos de nuevo la vista:
        res.render('login');
        return;
      }

      // apuntar a la sesion de usuario, que esta autenticado
      req.session.usuarioLogueado = usuario._id; // Este es el id de la bd.

      // mandamos un email al usuario: Recibe 2 parametros, asunto y el cuerpo del correo:
      usuario.sendEmail('Bienvenido', 'Bienvenido a NodeApp');

      // Si lo encuentra y la contraseña bien -> redirigimos a zona privada:
      res.redirect('/private');
    } catch (err) {
      next(err);
    }
  }

  // Método para hacer logout:
  logout(req, res, next) {
    // Este método de session nos permite matar la session
    req.session.regenerate((err) => {
      if (err) {
        next(err);
        return;
      }
      res.redirect('/');
    });
  }

  // Método para el JWT de la api:
  async postJWT(req, res, next) {
    // console.log(req.body)
    try {
      // Extraemos los datos del body:
      const { email, password } = req.body;

      // Comprobamos que exista en bd el usuario:
      const usuario = await Usuario.findOne({
        email: email,
      });

      // Si no lo encuentra o contraseña mal retorno -> error:
      if (!usuario || !(await usuario.comparePassword(password))) {
        // Respondemos con un json:
        res.json({ error: 'Invalid credentials' });
        return;
      }

      // si existe y la constraseña coincide --> devuelvo un JWT:
      // el metodo sing recibe tres parametros, 1 un {} con lo deseamos enviar, 2 la clave secreta, es un valor aleatoro puesto por nosotros, 3 un objeto de opciones con la expiracion del token:
      const tokenJWT = await jwt.sign(
        { _id: usuario._id },
        process.env.JWT_SECRET,
        {
          expiresIn: '2h',
        }
      );

      // Devolvemos el token en un {} en formato json:
      res.json({ jwt: tokenJWT });
    } catch (err) {
      next(err);
    }
  }
}

module.exports = LoginController;
