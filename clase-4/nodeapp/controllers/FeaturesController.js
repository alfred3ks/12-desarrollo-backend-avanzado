/*
Creamos nuestros controladores con clases:
*/
class FeaturesController {
  index(req, res, next) {
    res.render('features');
  }
}

module.exports = FeaturesController;
