const mongoose = require('mongoose');
const nodemailer = require('nodemailer');

// Cargamos bcrypt:
const bcrypt = require('bcrypt');

// Cargamos el transport para enviar email:
const emailTransportConfigure = require('../lib/emailTransportConfigure');

// creamos esquema:
const usuarioSchema = mongoose.Schema({
  email: { type: String, unique: true },
  password: String,
});

// Creamos el método estatico para el hash del password:
usuarioSchema.statics.hashPassword = function (passwordEnClaro) {
  return bcrypt.hash(passwordEnClaro, 7);
};

// Creamos un método de instanacia que comprueba la password de una usuario:
usuarioSchema.methods.comparePassword = function (passwordEnClaro) {
  return bcrypt.compare(passwordEnClaro, this.password);
};

// Método para enviar correos al usuario:
usuarioSchema.methods.sendEmail = async function (asunto, cuerpo) {
  // crear un transport:
  const transport = await emailTransportConfigure();

  // enviar email:
  const result = await transport.sendMail({
    from: process.env.EMAIL_SERVICE_FROM,
    to: this.email,
    subject: asunto,
    // text: -> email para texto plano,
    html: cuerpo,
  });
  console.log(
    `URL de previsualización: ${nodemailer.getTestMessageUrl(result)}`
  );
  return result;
};

// creamos el modelo:
const Usuario = mongoose.model('Usuario', usuarioSchema);

// exportamos el modelo
module.exports = Usuario;
