'use-strict';

// Cargamos la libreria amqplib:
const amqplib = require('amqplib');
const EXCHANGE = 'task-request';

//dotenv:
require('dotenv').config();

// Funcion sleep:
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

// Creamos una funcion main() y aqui la invocamos, como es asincrona lo hacemos asi:
main().catch((err) => console.log('Hubo un error: ', err));

async function main() {
  // Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
  const connection = await amqplib.connect(process.env.RABBITMQ_BROKER_URL);

  // Crear un canal:
  const canal = await connection.createChannel();

  // asegurar que existe un exchange:
  await canal.assertExchange(EXCHANGE, 'direct', {
    durable: true, //the exchange will survive broker restarts.
  });

  // Variable para constrolar la publicacion para evitar saturacion: Si es true puede seguir enviando si pasa a false es porque se ha saturado.
  let keepSending = true;

  while (true) {
    // publicar mensaje:
    const mensaje = {
      tarea: 'enviar un email ' + Date.now(),
    };

    // Verificamos si podemos enviar mas o tengo que esperar: Asi contenemos la publicacion:
    if (!keepSending) {
      // Nos suscribimos a un evento: Espera a que se drene (vacie) el buffer de escritura del broker:
      console.log('Buffer lleno, espero a que se vacie');
      await new Promise((resolve) => canal.on('drain', resolve));
    }

    keepSending = canal.publish(
      EXCHANGE,
      '*',
      Buffer.from(JSON.stringify(mensaje)),
      {
        persistent: true, // the message will survive broker restarts
      }
    );

    console.log('Enviado mensaje', mensaje);

    // Usamos la funcion sleep:
    await sleep(100);
  }
}
