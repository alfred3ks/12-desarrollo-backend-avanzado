'use-strict';

// Cargamos la libreria amqplib:
const amqplib = require('amqplib');

//dotenv:
require('dotenv').config();

// definimos la cola:
const QUEUE = 'tasks';

// Creamos una funcion main() y aqui la invocamos, como es asincrona lo hacemos asi:
main().catch((err) => console.log('Hubo un error: ', err));
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

async function main() {
  // Conectamos al broker de rabbitMQ: La url viene de la configuracion de cloudmqp:
  const connection = await amqplib.connect(process.env.RABBITMQ_BROKER_URL);

  // Crear un canal:
  const canal = await connection.createChannel();

  // asegurarnos que existe la cola donde vamos a recibir los mensajes:
  await canal.assertQueue(QUEUE, {
    durable: true, // the queue will surviver broken restarts
  });

  // le decimos a la cola que esta trabajando para evitar saturacion: Lo hara de uno en uno. Dame un mensaje y hasta que no lo procese no me des mas. Recibe por parametros el numero de msj:
  canal.prefetch(1); // pending ack

  // Ahora consumimos los mensajes:
  canal.consume(QUEUE, async (mensaje) => {
    const payload = mensaje.content.toString();
    console.log(payload);
    await sleep(150);
    canal.ack(mensaje);
  });
}
