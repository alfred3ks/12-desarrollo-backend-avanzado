# Clase 4: Tareas en segundo plano.

Cuando tenemos una petición cual tarda mas de lo normal.

En estudios realizados toda petición que tarda mas de un segundo ya pierden dinero. Es muy importante que las peticiones se respondan lo mas pronto posible, pero si esa gestion va a tardar debemos gestionar la gestion de ese tiempo, debemos darle fitback de lo que se esta haciendo. Esto es muy importante a dia de hoy.

Vamos a implementar un ejemplo como por ejemplo enviar un correo electronico. Vamos a ver como enviar un email desde Node.js.

## Enviar correo desde Node.js:

Existen multiples librerias para esto, nosotros vamos a usar esta:

```bash
npm repo nodemailer
```

https://github.com/nodemailer/nodemailer

Esta libreria envia mails en node.js.

Aqui podemos ver su documentación:

https://nodemailer.com/

La instalamos:

```bash
npm i nodemailer
```

Esta libreria es bastante versatil, como podriamos conectarnos aplataformas de envios de correos, tenemos varias como estas:

https://sendgrid.com/en-us

Tiene un pnan gratis para desarrolladores que podemos enviar 100 mail al dia. Bastante genial para hacer testing.

Tambien tenemos:

https://www.brevo.com/es/

Tambien tenemos a:

https://www.mailgun.com/es/

Pero porque hablamos de estos servicios? Hoy en dia el gran problema del correo es el spam, mandar un correo no hay problema pero enviar muchos correos ya si lo es, los servidores a dia de hoy se protegen contra esta forma de enviar correos masivos. Los servidores de correos tienen la mision de tratar de reducir esa cantidad e envio de spam.

En base a esto hay dos tipos de correos los transaccionales y los correos de marketing. Cada uno de estos servicios vistos antes vemos que estos son los servicios que ofrencen para email de marketing y email transaccionales. Los email transaccionales son por ejemplo cuando estramos a twitter, nos loguemos y nos avisan que acabamos de entrar, o pedir cambio de contraseña, luego estan los de marketing que son los que nos informan servicios.

La libreria que hemos instalado permite conectarnos con plataformas como hemos visto arriba. Tambien podemos usar nuestro correo de gmail. O cualquier otro servidor smtp.

Nodemailer tambien tiene un servicio que nos permite ver como se envian un correo pero realmente no se envia, es para el tema de desarrollo.

https://ethereal.email/

Vamos a usar este servicio.

Lo que haremos es que cuando el usuario haga login mandaremos un email.

Cuando el usuario haga login en el frontend enviaremos ese email, para eso trabajaremos en el controlador LoginController.js:

Creamos en el modelos de usuario la funcion sendMail(), que recibira 2 parametros uno el asunto del correo y otro el cuerpo del correo.

## Usuario.js:

```javascript
// Cargamos el transport para enviar email:
const emailTransportConfigure = require('../lib/emailTransportConfigure');
const nodemailer = require('nodemailer');

// Método para enviar correos al usuario:
usuarioSchema.methods.sendEmail = async function (asunto, cuerpo) {
  // crear un transport:
  const transport = await emailTransportConfigure();

  // enviar email:
  const sendEmail = await transport.sendMail({
    from: process.env.EMAIL_SERVICE_FROM,
    to: this.email,
    subject: asunto,
    // text: -> email para texto plano,
    html: cuerpo,
  });
  console.log(
    `URL de previsualización ${nodemailer.getTestMessageUrl(sendEmail)}`
  );
  return sendEmail;
};
```

## emailTransportConfigure.js.js:

```javascript
// Cargamos la libreria:
const nodemailer = require('nodemailer');

module.exports = async function () {
  // Entorno de desarrollo:
  // Creamos una cuenta de prueba:
  const testAccount = await nodemailer.createTestAccount();

  const developmentTransport = {
    host: testAccount.smtp.host,
    port: testAccount.smtp.port,
    secure: testAccount.smtp.secure,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  };

  const transport = nodemailer.createTransport(developmentTransport);

  return transport;
  // Entorno de producion:
};
```

## LoginController.js:

```javascript
// mandamos un email al usuario: Recibe 2 parametros, asunto y el cuerpo del correo:
const emailResult = await usuario.sendEmail(
  'Bienvenido',
  'Bienvenido a NodeApp'
);

console.log('Email enviado.', emailResult);

// Si lo encuentra y la contraseña bien -> redirigimos a zona privada:
res.redirect('/private');
```

Lo probamos. Vemos la salida por consola:

![](./img/mail-console.png)

Vemos la url:

https://ethereal.email/message/ZZm.Gb1VE-Ml3hwgZZm.Git8Op-uUNBMAAAAAcNRF9TWVd9XC6Ly4N5QWrI

Y tenemos el mensaje del correo enviando:

![](./img/mail-url.png)

Funciona...

🎯 Commit: Clase 4: Enviar correo en Node.js en desarrollo.

Ahora vamos a ver como seria prepararlo para que envie correos cuando este la aplicacion en producción:

## Envio de correo en producción:

Donde vamos a trabajar es en el módulo de configuracion del transport, el que hemos creado en
lib/emailTransportConfigure.js:

## emailTransportConfigure.js:

```javascript
// Cargamos la libreria:
const nodemailer = require('nodemailer');

module.exports = async function () {
  // Entorno de desarrollo:
  // Creamos una cuenta de prueba:
  const testAccount = await nodemailer.createTestAccount();

  const developmentTransport = {
    host: testAccount.smtp.host,
    port: testAccount.smtp.port,
    secure: testAccount.smtp.secure,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  };

  // Entorno de producion: Vamos a usar un servicio externo:
  const productionTransport = {
    service: process.env.EMAIL_SERVICE_NAME,
    auth: {
      user: process.env.EMAIL_SERVICE_USER,
      pass: process.env.EMAIL_SERVICE_PASS,
    },
  };

  // Usamos la configuración del entorno en que se ejecuta la aplicación:
  const activeTransport =
    process.env.NODE_ENV === 'development'
      ? developmentTransport
      : productionTransport;

  const transport = nodemailer.createTransport(activeTransport);

  return transport;
};
```

Para esto tambien hemos definido en los script de arranque de la aplicación estableciendo la variable de entorno process.env:NODE_ENV a si se arranca en produccion establezca eso y si es desarrollo la establezca en desarrollo:

## package.json:

```json

  "scripts": {
    "start": "cross-env node NODE_ENV=production ./bin/www",
    "dev": "cross-env NODE_ENV=development DEBUG=nodeapp:* nodemon ./bin/www",
  },
```

Tambien hemos establecido las variables de entorno en .env y .env-example:

## .env:

```bash
MONGODB_URI=mongodb://127.0.0.1/cursonode
JWT_SECRET=s876ads87dasuytasduytasd
EMAIL_SERVICE_FROM=admin@example.com
EMAIL_SERVICE_NAME=
EMAIL_SERVICE_USER=
EMAIL_SERVICE_PASS=
```

## .env-example:

```bash
MONGODB_URI=mongodb://127.0.0.1/nombre_bd
JWT_SECRET=secret
EMAIL_SERVICE_FROM=user@domain.com
EMAIL_SERVICE_NAME=(sendgrid, sendinblue, mailgun)
EMAIL_SERVICE_USER=
EMAIL_SERVICE_PASS=
```

NOTA: Como podemos ver faltaria configurar las variables de entorno para el servicio externo de envio de correo.

🎯 Commit: Clase 4: Enviar correo en Node.js en producción.
