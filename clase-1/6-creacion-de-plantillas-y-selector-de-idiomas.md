### Clase 1: Creación de plantillas: selector de idiomas:

Ahora lo que haremos es poner un selector de idioma a nuestra app. Es lo mas habitual ver esto. Para eso vamos a ver como usar un plantilla ya realizado por otro persona asi veremos como usar plantillas. Vamos a ver como meter una plantilla en una app de express.

La página donde podemos sacar plantillas:

https://startbootstrap.com/

Y el que vamos a usar:

https://startbootstrap.com/theme/new-age

Descargamos un .zip lo descomprimimos y lo metemos en la raiz del proyecto. Ahora dice que lo que tiene en la carpeta public se lo va a cargar porque ahi no tenemos nada que usemos. Y la carpeta de la pantilla descargada la llamamos public.

Recargamos la página y ya vemos como nos carga ahora el template que hemos cargado nosotros. esto pasa porque al hacerse una peticion a la raiz de la aplicacion, eso porque asi lo tenemos definido en app.js:

En los middleware:

```javascript
app.use(express.static(path.join(__dirname, 'public')));
```

Ahora el archivo index.html de la carpeta public es el que vamos a usar como referencia.
Vamos a modificar el archivo cabecera.ejs de las vistas poniendo el codigo del template descargado. Crearemos un footer.ejs y vamos a modificar el index.ejs.

Tambien vamos a crear una pagina mas, ya tenemos el home cuando se hace una petición a localhost:3000 se carga el index. Ahora vamos a poner otra pagina más.

Dentro de routes creamos features.js:

### routes/features.js:

```javascript
const express = require('express');

const router = express.Router();

router.get('/', (req, res, next) => {
  res.render('features');
});

module.exports = router;
```

Ahora creamos la vista: dentro de views:

### view/features.ejs:

```html
<% include cabecera.ejs %>
<!-- Basic features section-->
<section class="bg-light">
  <div class="container px-5">
    <div
      class="row gx-5 align-items-center justify-content-center justify-content-lg-between"
    >
      <div class="col-12 col-lg-5">
        <h2 class="display-4 lh-1 mb-4">Enter a new age of web design</h2>
        <p class="lead fw-normal text-muted mb-5 mb-lg-0">
          This section is perfect for featuring some information about your
          application, why it was built, the problem it solves, or anything
          else! There's plenty of space for text here, so don't worry about
          writing too much.
        </p>
      </div>
      <div class="col-sm-8 col-md-6">
        <div class="px-5 px-sm-0">
          <img
            class="img-fluid rounded-circle"
            src="https://source.unsplash.com/u8Jn2rzYIps/900x900"
            alt="..."
          />
        </div>
      </div>
    </div>
  </div>
</section>
<% include pie.ejs %>
```

Y en app.js le decimos la ruta:

### app.js:

```javascript
/**
 * Rutas del website
 */
// Middleware de los idiomas a usar para cada peticion:
app.use(i18n.init);
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/features', require('./routes/features'));
```

Recargamos y vemos que ya carga esa vista cuando hacemos localhost:3000/features, ademas a aprovechado para colocar varias traducciones, tanto al titulo como a las caracateristicas.

Ahora vamos a colocar nestro boton para seleccionar idiomas: Lo pondremos al lado del titulo de la web un slector de idiomas. Para esto vamos a usar una funcion de i18n que nos permite acceder a la configuracion de i18n, lib/i18nConfigure.js, y hacer dinamico los idiomas, si mañana agregamos otro idioma este se mostrara de manera dinamica.

### cabecera.ejs:

```html
<!-- Funcion para hacer dinamico el menu en funcion de los que tengamos en la configuracion de i18n -->
<% getLocales().forEach(locale=> {%>
<a href=""> <%=locale%> </a>&nbsp <%} )%>
```

Ahora lo que vamos a hacer es decirle a i18n que no haga caso a lo que le diga el navegador en la cabecera en el tema de idiomas y le haga caso a lo que se pulse en los selectores de idioma que acabamos de colocar. Para esto cuando hagamos click en un idioma haremos una peticion al backend este le enviara una cookie y i18n leera esa cookie y seleccionara el idioma y se olvidara de la cabecera del navegador. Con solo ponerlo en la configuracion i18n mirara primero siempre si hay una cookie que diga que idioma usar, si no lo hay usa la cabecera del navegador.

Para eso vamos lib/i18nConfigure.js y agregamos la configuracion de cookie para que acepte cookies:

```javascript
// cargar librerias
const i18n = require('i18n');
const path = require('node:path');

// configurar la instancia de la libreria
i18n.configure({
  locales: ['en', 'es'],
  directory: path.join(__dirname, '..', 'locales'),
  defaultLocale: 'en',
  autoReload: true, // watch for changes in JSON files to reload locale on updates - defaults to false
  syncFiles: true, // sync locale information across all files - defaults to false
  cookie: 'nodeapp-locale',
});

// para utilizar un scripts
i18n.setLocale('en');

// exportar
module.exports = i18n;
```

Ahora vamos a crear la cookie.

### Cookie de idioma:

En la cabecera vamos a llamar a un metodo que vamos a crear:

```html
<% getLocales().forEach(locale=> {%>
<a href="/change-locale/<%= locale %>"> <%=locale%> </a>&nbsp <%})%>
```

Ahora si pulsamos en los selectores de idiomas vemos que se va a la ruta:

http://localhost:3000/change-locale/en
http://localhost:3000/change-locale/es

Hasta ahora vamos bien. Seguimos...

Ahora tenemos que hacer que el servidor cuando reciba esta peticion responda a la misma pagina que estaba pero poniendo una cookie de idioma.

Nos vamos a la carpeta de routes y creamos change-locale.js

### routes/change-locale.js:

```javascript
const express = require('express');
const router = express.Router();

// GET /change-locale/[locale]
router.get('/:locale', (req, res, next) => {
  const locale = req.params.locale;
  // poner una cookie con el nuevo idioma
  res.cookie('nodeapp-locale', locale, {
    maxAge: 1000 * 60 * 60 * 24 * 30, // 30 dias
  });

  // repondemos con una redireccion a la misma pagina de la que venia:
  // referer lo vemos que lo da la peticion cuando vamos de una pagina a otra lo vemos en las devtoools
  res.redirect(req.get('referer'));
});

module.exports = router;
```

Ahora en app.js creamos la ruta:

```javascript
/**
 * Rutas del website
 */
// Middleware de los idiomas a usar para cada peticion:
app.use(i18n.init);
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/features', require('./routes/features'));
app.use('/change-locale', require('./routes/change-locale'));
```

Lo probamos y funciona!!!! Vemos como tenemos una cookie. Perfecto. Esto por hoy.
