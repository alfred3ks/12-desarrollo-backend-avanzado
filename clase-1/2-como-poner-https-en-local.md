## Clase 1: Como poner HTTPS en mi entorno local:

Esto para desarrollo no es lo mas común pero veremos como hacerlo. Vamos a ver disitintas formas de hacerlo.

Veremos estas tres formas:

Manual:
https://www.freecodecamp.org/news/how-to-get-https-working-on-your-local-development-environment-in-5-minutes-7af615770eec/

shell:
https://github.com/dakshshah96/local-cert-generator

mkcert:
https://github.com/FiloSottile/mkcert

Lo que necesitamos es que el browser confie en nuestro servidor. Para esto necesitamos un certificado.

El browser hace una petición a un servidor y este le devuelve un certificado. El browser evalua ese certificado para saber si se puede fiar o no. Lo hace comprobando si este esta emitido por un organismo de confianza, tipo casa de la moneda, un entidad que el browser se fia. Existe un directorio de entidades certificadoras.

Una de ellas es Let`s Encrypt. Lo podemos ver en el navegador cuando visitamos las páginas que entidad emite el certificado:

![](./img/https-1.png)

Vamos a crear un entidad certificadora en nuestra máquina, decirle al browser quew confie en ella, y luego cons esa entidad certificadora nos creamos un certificado. Eso es lo que explica el articulo de freecodecamp.

Existe otras formas mas comodas pàra hacerlo lo vemos en el repo que tenemos como shell. Es lo mismo que lo uqe hace en freecodecamp pero este lo hace mas con un script que lo hace todo mas en automatico.

Tambien existe otra herramienta llamada mkcert, es otro repo. Que tambien nos dice como hacerlo.

Mas formas de hacerlo tenemos una herramienta que se llama ngrok. Lo vemos en el pdf de la clase.

https://ngrok.com/

si usamos esta alternativa cuando arancamos nuestro app en el puerto que tenga asigando crea un proxi, todas la peticiones que le hacemos a ngrok luego las redirige a nuestro puerto. Esto nos evita crearnos cosas en nuestra máquina.

Pero veremos otra herramienta mucho mas sencilla, localtunne. El preferido del profesor. Tenemos el siguiente repo:

https://github.com/localtunnel/localtunnel

De usar es muy facil y no tenemos que instalar nada porque usaremos npx:

```bash
npx localtunnel --p 3000

npx localtunnel --p 3000 --subdomain jamg44
```

Vamos a probarla seguiremos con la aplicacion que se uso en fundamentos.

nodeapp.

Para los de windows recordar que el servidor de bd de mongodb ya esta arrancado.

Y ahora arracamos nuestro nodeapp.

```bash
npm run dev
```

Verificamos que este arrancada: localhost:3000

Ahora para ver corriendo la aplicación usando localtunnel:
Abrimos una consola nueva y corremos el comando:

```bash
npx localtunnel --p 3000
```

Esto nos genera una url que es la que debemos visitar:

https://smooth-taxes-swim.loca.lt/

Al visitar es url nos muestra unas instrucciones donde debemos colocar nuestra ip, para saber nuestra ip podemos usar el servicio llamado What is my ip.

![](./img/what-is-my-ip.png)

Una ves que tenemos la IP nos vamos a la url de que nos da localtunne y pegamos la ip:

![](./img/what-is-my-ip2.png)

Ahora le damos click al boton azul y vemos como nuestra app ya esta corriendo usando https en modo de desarrollo.

![](./img/what-is-my-ip3.png)

Con localtunnel podemos tambien generar subdominios solo le debemos decir los siguiente:

```bash
npx localtunnel --p 3000 --subdomain nodeapp
```

Ahora ya la aplicacion estaria corriendo usando el subdominio que le pasemos

![](./img/subdomio.png)

Tambien vemos que funciona con la api:

https://nodeapp.loca.lt/api/agentes

user:admin
clave:12345
