const express = require('express');
const router = express.Router();

// GET /change-locale/[locale]
router.get('/:locale', (req, res, next) => {
  const locale = req.params.locale;
  // poner una cookie con el nuevo idioma
  res.cookie('nodeapp-locale', locale, {
    maxAge: 1000 * 60 * 60 * 24 * 30, // 30 dias
  });

  // repondemos con una redireccion a la misma pagina de la que venia:
  // referer lo vemos que lo da la peticion cuando vamos de una pagina a otra lo vemos en las devtoools
  res.redirect(req.get('referer'));
});

module.exports = router;
