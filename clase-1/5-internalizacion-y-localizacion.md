## Clase 1: Internacionalización y localización: Min 1:43:21

Nuestra web en varios idiomas. Esto tambien se puede llamar multi idioma. Cuando hablamos de internalización es hacer que nuestra aplicación funcione con distintos idiomas sin cambiar su código.

Una misma base de código por ejemplo en español puede trabajar en ingles, aleman, etc.

Cuando hablamos de localización, hablamos de añadir un idioma más a esa aplicación.

El objetivo de la internacionalización y la localización es permitir a un único sitio web ofrecer sus contenidos en diferentes idiomas y formatos adaptados a su audiencia.
No sólo vale con traducir, también hay que formatear los contenidos en función de las preferencias del usuario.

Por ejemplo las fechas en diferentes paises:

Formato de fecha España: dd/mm/yyyy
Formato de fecha USA: mm/dd/yyyy

• Internacionalización: preparar el software para que sea localizable (trabajo de desarrolladores).

• Localización: escribir la traducción y los formatos locales (suele ser trabajo de traductores).

-- Descanzo: 21:01 volvemos a 21:25

### Los pasos a realizar lo tenemos en el pdf, página 35. Lo vemos a continuación:

- Instalamos en nuestra app el paquete o dependencia o libreria llamada i18n.

A nivel curiosidad i de inter, 18 las letras del medio y n de nacionalizacion.

```bash
npm install i18n
```

El repo:

https://github.com/mashpie/i18n-node

Ahora vamos a la carpeta lib y creamos un nuevo fichero llamado i18nConfigure.js

```javascript
// cargar librerias
const i18n = require('i18n');
const path = require('node:path');

// configurar la instancia de la libreria
i18n.configure({
  locales: ['en', 'es'],
  directory: path.join(__dirname, '..', 'locales'),
  defaultLocale: 'en',
  autoReload: true, // watch for changes in JSON files to reload locale on updates - defaults to false
  syncFiles: true, // sync locale information across all files - defaults to false
});

// para utilizar un scripts
i18n.setLocale('en');

// exportar
module.exports = i18n;
```

Tambien debemos crear la carpeta locales donde iran los archivos de traduccion, la ponemos en la raiz del proyecto.

Lo siguiente es cargar en app.js el módulo que hemos creado de i18n. Lo que haremos es crear un middleware. OJO cargamos como primer middleware para que evalue eso y busque en las cabeceras de la peticion el idioma que le manda el usuario segun su navegador tenga configurado.

```javascript
// Cargamos i18n para la internalización:
const i18n = require('./lib/i18nConfigure');

/**
 * Rutas del website
 */
// Middleware de los idiomas a usar para cada peticion:
app.use(i18n.init);
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
```

Ahora vamos a utilizarlo para eso nos vamos a las vistas. Carpeta views y vamos a usarlo en la index.ejs.

Esto es lo que tenemos:

### views/index.ejs:

```ejs
<p>Welcome to <%= title %></p>
```

Y cuando usamos i18n: Llamamos un funcion que es i18n con el \_\_()

### views/index.ejs:

```ejs
<p><%= __('Welcome to')%> <%=title %> </p>
```

Esto lo que hace ahora es buscar ese literal que le pasamos en los ficheros de idioma y traducirlo. Al ejecutarlo osea refrescar la pagina se nos crearon dos ficheros .json dentro de locales, los dos ficheros porque eso le hemos dicho en la cofiguracion de i18n.

Vemos los dos json:

### locales/es.json

```json
{
  "Welcome to": "Bienvenido a"
}
```

### locales/en.json

```json
{
  "Welcome to": "Welcome to"
}
```

Ya lo tenemos ahora lo podemos usar en cualquier archivo que deseemos aplicar la traducción. Por ejemplo en la cabecera:

### views/cabecera.ejs:

```ejs
<h1>
  <%=__('HEADER')%>
</h1>
```

Y los archivos de traduccion en locales nos quedaria asi:

### locales/es.json

```json
{
  "Welcome to": "Bienvenido a",
  "HEADER": "CABECERA"
}
```

### locales/en.json

```json
{
  "Welcome to": "Welcome to",
  "HEADER": "HEADER"
}
```

NOTA: La traducción la tenemos que poner nosotros en el archivo .json de la carpeta locales.

Ahora veremos que la libreria i8n nos ayuda a muchas cosas mas, como por ejemplo los plurales. En ingles no son iguales a como es el español. Ya hemos visto el metodo **(), para el tema de la pluralización vemos que tenemos el método **n(). Lo podemos ver en la documentación.

Vemos un ejemplo de las vistas. En index.ejs.

### views/index.ejs:

```html
<h2><%=__('i18n example for plurals')%></h2>
<p>Hay 0 <%=__n('Mouse', 0)%></p>
<p>Hay 1 <%=__n('Mouse', 1)%></p>
<p>Hay 2 <%=__n('Mouse', 2)%></p>
<p>Hay 3 <%=__n('Mouse', 3)%></p>
<p>Hay 4 <%=__n('Mouse', 4)%></p>
<p>Hay 5 <%=__n('Mouse', 5)%></p>
```

### locales/es.json

```json
{
  "Welcome to": "Bienvenido a",
  "HEADER": "CABECERA",
  "Conditionals": "Condicionales",
  "Loops": "Bucles",
  "i18n example for plurals": "Ejemplo i18n para el plural",
  "Mouse": {
    "one": "Ratón",
    "other": "Ratones"
  }
}
```

### locales/en.json

```json
{
  "Welcome to": "Welcome to",
  "HEADER": "HEADER",
  "conditionals": "conditionals",
  "Conditionals": "Conditionals",
  "Loops": "Loops",
  "i18n example for plurals": "i18n example for plurals",
  "Mouse": {
    "one": "Mouse",
    "other": "Mice"
  }
}
```

Hasta ahora hemos visto como usar los métodos de internalización en las vistas, pero tambien las podemos usar en los controladores.

Nos vamos la controlador que sirve el index.ejs de las vistas que es el que esta en routes/index.js.

Paras poder usar la funcion \_\_() no la podemos usar asi como asi, para eso i18n lo ha cargado sobre el objeto res, lo tendriamos que usar asi res.\_\_('texto').

### routes/index.js:

```javascript
res.locals.texto = res.__('Hello');
```

### locales/es.json

```json
{
  "Welcome to": "Bienvenido a",
  "HEADER": "CABECERA",
  "Conditionals": "Condicionales",
  "Loops": "Bucles",
  "i18n example for plurals": "Ejemplo i18n para el plural",
  "Mouse": {
    "one": "Ratón",
    "other": "Ratones"
  },
  "Hello": "Hola"
}
```
