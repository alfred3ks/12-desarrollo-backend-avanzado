## Clase 1: Cluster: Producción:

Veremos como poner nuestra app en modo cluster, para que sirve como me beneficia.

El modo cluster es un modo de ejecución que tiene node.js y sirve para que arranque mi aplicación varias veces. Node-js usa un solo hilo por defecto para ejecutar la aplicacion. aqui tenemos que ver los nucles que tiene el pc donde vamos a instalar la aplicacion. Normalmente un nucleo de cpu ejecuta dos hilos. Con un cluster podemos hacer que nuestra aplicacion use esos hilos y puede soportar mas peticiones en paralelo. si mi maquina tiene 8 cores arrancara la aplicacion 8 veces y ademas unifica el punto de entrada de las peticiones y las distribuye a donde esta arrancada.

Vemos como poner en modo cluster nuestra aplicacion: OJO para desarrollo no, esto es ideal usarlo ya en producción. En nuestra aplicacion vamos a crear una copia del fichero www de la carpeta bin y la llamaremos cluster.

Para arrancar la aplicacion:

```bash
node ./bin/cluster
```

Asi vemos como arrancar un worker:

### cluster

```javascript
// Cargamos el módulo cluster:
const cluster = require('node:cluster');
// Siempre arranca en primario:
if (cluster.isPrymary) {
  // arrancar los workers:
  cluster.fork();
} else {
  var port = normalizePort(process.env.PORT || '3000');
  app.set('port', port);

  var server = http.createServer(app);

  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);
}
```

Ahora lo que haremos es arrancar tantos como sea capaz mi máquina, para eso vamos a usar una libreria que tiene node.js llamada os:

```javascript
// Cargamos el módulo cluster:
const cluster = require('node:cluster');

// Cargamos os para ver los nucles de la pc:
const os = require('node:os');

// Siempre arranca en primario:
if (cluster.isPrimary) {
  console.log('arrancando worker primario');
  const numCores = os.cpus().length;
  console.log(`Número de CPUs: ${numCores}`);
  for (let i = 0; i < numCores; i++) {
    // arrancar los workers:
    cluster.fork();
  }

  // Emitimos eventos usando la liberia cluster:
  cluster.on('listening', (worker, address) => {
    console.log(
      `Worker ${worker.id} arrancado en con PID ${worker.process.pid}`
    );
  });

  cluster.on('exit', (worker, code, signal) => {
    console.log(
      `Worker ${worker.id} arrancado en con PID ${worker.process.pid} se ha parado con código ${code} y signal ${signal}`
    );
  });

  // si soy primary no hago nada mas
} else {
  // Soy un worker, por lo tanto me pongo a atender peticiones:

  /**
   * Get port from environment and store in Express.
   */
  var port = normalizePort(process.env.PORT || '3000');
  app.set('port', port);

  /**
   * Create HTTP server.
   */

  var server = http.createServer(app);

  /**
   * Listen on provided port, on all network interfaces.
   */

  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);
}
```

Para ver los procesos arrancados lo podemos ver por medio del administrador de tareas. Los veremos que estan corriendo con el nombre node. Ahi podemos ver el PID. Ahi vemos como esta corriendo nuestra app en cada nucleo.

Si matamos los procesos vemos que la aplicacion sigue funcionando, claro esta hasta que matemos el ultimo proceso.

Aunque si matamos un proceso podemos generar otro, eso lo hacemos cuando se genera el evento de exit:

```javascript
cluster.on('exit', (worker, code, signal) => {
  console.log(
    `Worker ${worker.id} arrancado en con PID ${worker.process.pid} se ha parado con código ${code} y signal ${signal}`
  );

  console.log('Arranco otro cluster');
  cluster.fork();
});
```

Esto esta bien para mantener los worker arrancados, suele venir bien al actualizar la aplicación en producción. Podemos ir matando worker que se arracaran con la nueva version. Eso se puede automatizar.

El módulo cluster reparte las peticiones que llegan al puerto de nuesta aplicación usando la logica que se llama 'round robin'. es una lógica de balanceo de carga. Existen otras pero el modo cluster usa esta.

La pregunta del millon, ¿cuando conviene usar nuestra app como cluster?

R: La cantidad de cpu que voy a utilizar va en funcion de las peticiones que va a recibir nuestra aplicacion, lo vemos en el siguiente cuadro:

![](./img/cluster.png)

Podemos ver las peticiones concurrente y como seria sin usar cluster y usando un cluster. donde se ve que ya no hay una mejora sustancial es a partir de 8 workers, donde se ya se nota es a partir de 2 peticiones concurrentes.

Aqui no hay magia, lo que hacemos es el uso de mas CPU.

Para aplicaciones donde las peticiones que se reciben son principalmente se uso de red o de disco, un cluster no tiene mucho sentido, porque lo que interesa es mas la velocidad de lectura y escritura del disco.

En cambio en una aplicación que reciba peticiones que requieran bastante claculo de CPU el modelo de un cluster nos viene muy bien.

Para profundizar mas sobre cluster vemos la documentación de node.js y el pdf de la clase:

https://nodejs.org/api/cluster.html
