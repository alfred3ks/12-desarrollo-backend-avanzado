# Clase 1: módulo de backend avanzado.

Para este módulo usaremos el mismo repo del módulo inicial. Para este módulo la clase la tenemos en la carpeta slides-avanzado. Tambien tenemos la carpeta de practica donde esta la practica. Lo que pide en la practica se harta en clase.

Lo que veremos en este módulo:

- Desarrollo de la aplicación con https.
- Como poner la aplicación en modo cluster.
- Como hacer debuging o como hacer trasar de los fallos de la aplicación.
- Como extructurar los controladores para hacer mas sencillo el testing.
- Como hacer internalización de la aplicacion, multiidioma. aunque esto se suele hacer en el frontend.
- Veremos como meter template de terceros.
- Veremos como hacer autenticación. Como entenderla bien. Hacerla es facil.
- Veremos JWT, json web token.
- Como enviar email y tareas pesadas.
- Veremos websockets.
- Tambien veremos microservicios.
- PM2, herramienta para lanzar procesos de node.
