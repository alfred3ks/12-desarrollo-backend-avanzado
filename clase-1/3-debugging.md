## Clase 1: Debugging:

Existen diversar formas de hacer debugging de nuestra app. Una usar console.log(), esa es una. Pero dependera de lo que estemos haciendo.

Para el tema la consola, tenemos varios:

console.log()
console.info()
console.error()
console.warn()
console.time()

Aqui vemos el console.time() el cual nos permite medir cuanto tiempo tarda una parte del nuestro código.

```javascript
console.time('100mil_elementos');
for (var i = 0; i < 100000000; i++) {
  let a = 1;
  a = a * i;
}
console.timeEnd('100mil_elementos');
```

Tambien podemos ver el metodo .trace(), esto nos permite seguir las trazas de nuestro codigo:

```bash
console.trace("Traza");

Trace: Traza
at test (/Users/javi/traza.js:21:13)
at Object.<anonymous> (/Users/javi/traza.js:25:1)
at Module.\_compile (module.js:434:26)
at Object.Module.\_extensions..js (module.js:452:10)
at Module.load (module.js:355:32)
at Function.Module.\_load (module.js:310:12)
at Function.Module.runMain (module.js:475:10)
at startup (node.js:117:18)
at node.js:951:3
```

En Nodejs tambien la tenemos para hacer debug. Aunque se usa muy poco. si ejecutamos por consola el siguiente comando:

```bash
node debug prueba.js

< Debugger listening on port 5858
debug> . ok
break in prueba.js:1
> 1 "use strict";
 2
 3 let neo = { name: 'Thomas', age: 33, surname: 'Andreson'};
debug>
```

Algunos de los comandos que mas se usan con este modo debug de nodejs:

```bash
cont, c - Continue execution
next, n - Step next
step, s - Step in
out, o - Step out
pause - Pause running code
repl - Entra en modo evaluación (Ctrl+c para salir)
help - Muestra comandos
restart - Re-inicia aplicación
kill - Mata la aplicación
scripts - Muestra scripts cargados
```

Para profundizar mas vemos la documentación:

https://nodejs.org/api/debugger.html

Tambien podemos usar el inspector del navegador chrome pero debemos arrancar nuestra aplicacion usando el comando:

```bash
node --inspect-brk index.js
```

Vamos a probarlo en nuestra aplicación:

```bash
node --inspect-brk ./bin/www
```

Ahora para poder ejecutarlos debemos colocar en el navegador:

chrome://inspect

![](./img/debug.png)

Procedemos a darle donde dice inspect y nos apareceran las devtools. La sacara en una ventana flotante.

En la esquina superior izquierda de esta ventana flotante podemos agregar nuestra aplicacion, donde dice añadir carpeta:

![](./img/debug1.png)

Ahora ya podemos colocar los breadpoint que queramos. Recargamos la aplicacion y vemos como se para en el breakpoint. Hasta aqui en el navegador.

Tambien podemos hacer debug usando vscode. Lo vemos en el pdf de la clase.
