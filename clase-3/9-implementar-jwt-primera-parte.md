# Clase 3: Implementar JWT en nuestra app: Primera parte.

Primer parte de implementacion de JWT. Nos vamos ahora a la API.

Como estamos haciendo todo ya por controladores nos vamos a la carpeta controller y a LoginController.js:

Para generar el JWT usaremos una libreria externa llamada jsonwebtoken se podria hacer con nodejs pero lo ideal es usar la libreria externa.

```bash
npm repo jsonwebtoken
```

https://github.com/auth0/node-jsonwebtoken

Esta libreria esta respaldada por una gran empresa llamada Auth0.

La instalamos:

```bash
npm i jsonwebtoken
```

## LoginController.js: creamos un metodo nuevo para la api:

```javascript
// Importamos la libreria JWT:
const jwt = require('jsonwebtoken');

// Método para el JWT de la api:
  async postJWT(req, res, next) {
    // console.log(req.body)
    try {
      // Extraemos los datos del body:
      const { email, password } = req.body;

      // Comprobamos que exista en bd el usuario:
      const usuario = await Usuario.findOne({
        email: email,
      });

      // Si no lo encuentra o contraseña mal retorno -> error:
      if (!usuario || !(await usuario.comparePassword(password))) {
        // Respondemos con un json:
        res.json({ error: 'Invalid credentials' });
        return;
      }

      // si existe y la constraseña coincide --> devuelvo un JWT:
      // el metodo sing recibe tres parametros, 1 un {} con lo deseamos enviar, 2 la clave secreta, es un valor aleatoro puesto por nosotros, 3 un objeto de opciones con la expiracion del token:
      const tokenJWT = await jwt.sign(
        { _id: usuario._id },
        's876ads87dasuytasduytasd',
        {
          expiresIn: '2h',
        }
      );

      // Devolvemos el token en un {} en formato json:
      res.json({ jwt: tokenJWT });
    } catch (err) {
      next(err);
    }
  }
```

Vamos a probarlo a ver si retorna el token, vamos a las rutas d ela api en app.js y hacemos un cambio:

Tenemos protegida la ruta de la api con basic authentication se lo vamos a quitar porque la vamos a proteger con el JWT:

## app.js: asi antes:

```javascript
app.use('/api/agentes', basicAuthMiddleware, require('./routes/api/agentes'));
```

## app.js: asi ahora:

```javascript
// 📌 Creamos una instancia del controlador:
const loginController = new LoginController();

// middleware de JWT:
app.post('/api/login', loginController.postJWT);
app.use('/api/agentes', require('./routes/api/agentes'));
```

Para probarlo lo hacemos con postman:

![](./img/jwt2.png)

Funciona!!!!

🎯 Commit: Clase 3: Implementación de JWT primera parte.
