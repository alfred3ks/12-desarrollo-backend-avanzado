# Clase 3: Mostrar data por usuario.

Lo que veremos es como mostrar en la vista la información que dependera del usuario que esta logueado. Mostraremos los agentes que pertenecen a cada usuario logueado.
La información la debemos mostrar en la vista de zona privada.

Lo veremos en el controlador PrivateController.js

## PrivateController.js:

```javascript
// Cargamos los modelo de Agentes y Usuario :
const { Usuario, Agente } = require('../models');

// Obtenemos la lista de agentes de la bd que pertencen al usuario:
const agentes = await Agente.find({ owner: usuarioId });

// Renderizamos la vista y la variable local
res.render('private', { email: usuario.email, agentes });
```

Vemos como obtenemos los agentes en funcion del usuario y se lo pasamos a la vista private como una propiedad.

Ahora en la vista renderizamos:

## private.ejs

```html
<!-- Mostramos la lista de agentes -->
<div
  class="row gx-5 align-items-center justify-content-center justify-content-lg-between"
>
  <div class="col-12 col-lg-12">
    <table class="table">
      <thead>
        <tr>
          <th scope="col">ID usuario</th>
          <th scope="col">Agent</th>
          <th scope="col">Age</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        <% agentes .forEach((agente)=> { %>
        <tr>
          <th scope="row"><%=agente._id %></th>
          <td><%= agente.name %></td>
          <td><%= agente.age %></td>
          <td>delete</td>
        </tr>
        <% }) %>
      </tbody>
    </table>
  </div>
</div>
```

Para mostrar el contenido en tablas se ha usado esto de bootstraps:

https://getbootstrap.com/docs/5.3/content/tables/

🎯 Commit: Clase 3: Mostrar los agentes en zona privada en función del usuario logueado.
