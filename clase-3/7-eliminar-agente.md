# Clase 3: Eliminar agentes.

Lo que haremos es nuestra vista colocar un boton para eliminar un agente, lo que haremos es colocar un icon de bootstrap:

## private.ejs:

```html
<td>
  <a
    onclick="confirmDeleteAgent('<%= agente.name %>','<%= agente._id %>')"
    href="javascript:void(0)"
    class="btn btn-primary"
    ><i class="bi bi-trash"></i
  ></a>
</td>
```

Y usamos un script al pie de la página:

```javascript
// Creamos el script de confirmación
const confirmDeleteAgent = (name, agenteId) => {
  const mensaje = `Estas seguro que quieres eliminar el agente ${name}?`;
  if (confirm(mensaje)) {
    // Redirigimos a esta url:
    window.location.href = `/agentes-delete/${agenteId}`;
  }
};
```

Ahora vamos al controlador de Agentes AgenteController.js y creamos el metodo para eliminar un agente:

## AgenteController.js:

```javascript
  // Método para eliminar un agente recibiendo un id:
  async deleteAgent(req, res, next) {
    try {
      const usuarioId = req.session.usuarioLogueado;
      const agenteId = req.params.agenteId;

      // Validamos que el agente que quewremos borrar es propiedad del usuario:
      const agente = await Agente.findOne({ _id: agenteId });

      // Verificar que existe:
      if (!agente) {
        // Este console lo usamos como traza de seguridad:
        console.warn(
          `WARNING - el usuario ${usuarioId} intento eliminar eliminar un agente que no existe (${agenteId})`
        );
        next(createError(404, 'Not found'));
        return;
      }

      // Verificamos si el usuario es el que puede eliminar ese agente:
      // OJO concha de platano 🍌
      /*
      usuarioIdes un string,
      agente.owner es un objectId(mongodb), hay que transforar a uno de ellos sino siempre entraria en el if
      */
      if (agente.owner.toString() !== usuarioId) {
        // Este console lo usamos como traza de seguridad:
        console.warn(
          `WARNING - el usuario ${usuarioId} intento eliminar eliminar un agente (${agenteId}), de propiedad de otro usuario (${agente.owner})`
        );
        next(createError(401, 'No autorizado'));
        return;
      }

      // Borramos el usuario:
      await Agente.deleteOne({ _id: agenteId });

      // Y redireecionamos:
      res.redirect('/private');
    } catch (err) {
      next(err);
    }
  }
```

Y luego en app.js creamos la ruta:

## app.js:

```javascript
// Ruta para eliminar agentes: Protegido
// Ruta para eliminar agentes: Protegido
app.get(
  '/agentes-delete/:agenteId',
  sessionAuthMiddleware,
  agentesController.deleteAgent
);
```

Lo probamos y vemos que funciona correctamente, ya eliminamos agentes.

🎯 Commit: Clase 3: Eliminar agentes de la bd.
