var createError = require('http-errors');
const Agente = require('../models/Agente');

class AgentesController {
  // Creamos nuevos agentes
  new(req, res, next) {
    res.render('agentes-new');
  }

  // Metodo para crear un agente
  async postNewAgent(req, res, next) {
    try {
      const usuarioId = req.session.usuarioLogueado;
      const { name, age } = req.body;

      const agente = new Agente({
        name: name,
        age: age,
        owner: usuarioId,
      });

      await agente.save();

      // Lo rederigimos a la pagina de privado:
      res.redirect('/private');
    } catch (err) {
      next(err);
    }
  }

  // Método para eliminar un agente recibiendo un id:
  async deleteAgent(req, res, next) {
    try {
      const usuarioId = req.session.usuarioLogueado;
      const agenteId = req.params.agenteId;

      // Validamos que el agente que quewremos borrar es propiedad del usuario:
      const agente = await Agente.findOne({ _id: agenteId });

      // Verificar que existe:
      if (!agente) {
        // Este console lo usamos como traza de seguridad:
        console.warn(
          `WARNING - el usuario ${usuarioId} intento eliminar eliminar un agente que no existe (${agenteId})`
        );
        next(createError(404, 'Not found'));
        return;
      }

      // Verificamos si el usuario es el que puede eliminar ese agente:
      // OJO concha de platano 🍌
      /*
      usuarioIdes un string,
      agente.owner viene de la bd y es un objectId, hay que transformar a uno de ellos sino siempre entraria en el if
      */
      if (agente.owner.toString() !== usuarioId) {
        // Este console lo usamos como traza de seguridad:
        console.warn(
          `WARNING - el usuario ${usuarioId} intento eliminar eliminar un agente (${agenteId}), de propiedad de otro usuario (${agente.owner})`
        );
        next(createError(401, 'No autorizado'));
        return;
      }

      // Borramos el usuario:
      await Agente.deleteOne({ _id: agenteId });

      // Y redireecionamos:
      res.redirect('/private');
    } catch (err) {
      next(err);
    }
  }
}

module.exports = AgentesController;
