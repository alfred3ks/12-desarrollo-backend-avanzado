var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// Requerimos el modulo para hacer persistente la sesion en bd:
const MongoStore = require('connect-mongo');

// Cargamos la libreria session:
const session = require('express-session');

// Agrupamos los middleware:
const basicAuthMiddleware = require('./lib/basicAuthMiddleware');
const swaggerMiddleware = require('./lib/swaggerMiddleware');
const sessionAuthMiddleware = require('./lib/sessionAuthMiddleware');

// Cargamos i18n para la internalización:
const i18n = require('./lib/i18nConfigure');

// 📌 Cargamos los controladores:
const FeaturesController = require('./controllers/FeaturesController');
const LangController = require('./controllers/LangController');
const LoginController = require('./controllers/LoginController');
const PrivateController = require('./controllers/PrivateController');
const AgentesController = require('./controllers/AgentesController');

require('./lib/connectMongoose');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// definimos una variable de vista que estará disponible
// en todos los render que hagamos
app.locals.title = 'NodeApp';

// middlewares
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false })); // parea el body en formato urlencoded
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// app.use('/pdfs', express.static(path.join(__dirname, 'd:/pdfs')));

// app.use((req, res, next) => {
//   console.log('Ha llegado una petición a', req.url);
//   next('zzz');
// });

// 📌 Creamos una instancia del controlador:
const featuresController = new FeaturesController();
const langController = new LangController();
const loginController = new LoginController();
const privateController = new PrivateController();
const agentesController = new AgentesController();

/**
 * Rutas del API
 */
app.use('/api-doc', swaggerMiddleware);

// middleware de JWT:
app.post('/api/login', loginController.postJWT);
app.use('/api/agentes', require('./routes/api/agentes'));

/**
 * Rutas del website
 */
// Middleware de los idiomas a usar para cada peticion:
app.use(i18n.init);

// Cargamos el middleware de session:
app.use(
  session({
    name: 'nodeapp-session', // nombre de la cookie
    secret: 'asanjnvvldfngfsdljvfdjgfdg', // pisada de gato, esto es cualquier valor.
    saveUninitialized: true, // Forces a session that is "uninitialized" to be saved to the store.
    resave: false, // Forces the session to be saved back to the session store, even if the session was never modified during the request.
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 2, // Tiempo de expiración (ms) 2 dias, de la sesion por inactividad.
    },
    store: MongoStore.create({ mongoUrl: process.env.MONGODB_URI }),
  })
);

// Permitimos que las vistas accedan a la session: Hacemos que el objeto session este disponible al renderizar las vistas:
app.use((req, res, next) => {
  res.locals.session = req.session;
  next();
});

app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));

// 📌 Creamos la ruta de esta manera:
// app.use('/features', require('./routes/features'));
app.get('/features', featuresController.index);

// app.use('/change-locale', require('./routes/change-locale'));
app.get('/change-locale/:locale', langController.changeLocale);

// Ruta para el login:
app.get('/login', loginController.index);
app.post('/login', loginController.post);

// Ruta para hacer logout:
app.get('/logout', loginController.logout);

// Ruta para la pagina private:
app.get('/private', sessionAuthMiddleware, privateController.index);

// Ruta para crear agentes: Protegido
app.get('/agentes-new', sessionAuthMiddleware, agentesController.new);
app.post('/agentes-new', sessionAuthMiddleware, agentesController.postNewAgent);

// Ruta para eliminar agentes: Protegido
app.get(
  '/agentes-delete/:agenteId',
  sessionAuthMiddleware,
  agentesController.deleteAgent
);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // comprobar si es un error de validación
  if (err.array) {
    const errorInfo = err.errors[0]; // err.array({ onlyFirstError: true })[0]
    console.log(errorInfo);
    err.message = `Error en ${errorInfo.location}, parámetro ${errorInfo.path} ${errorInfo.msg}`;
    err.status = 422;
  }

  res.status(err.status || 500);

  // si lo que ha fallado es una petición al API
  // responder con un error en formato JSON
  if (req.originalUrl.startsWith('/api/')) {
    res.json({ error: err.message });
    return;
  }

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.render('error');
});

module.exports = app;
