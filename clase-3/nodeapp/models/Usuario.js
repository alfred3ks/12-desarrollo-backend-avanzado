const mongoose = require('mongoose');

// Cargamos bcrypt:
const bcrypt = require('bcrypt');

// creamos esquema:
const usuarioSchema = mongoose.Schema({
  email: { type: String, unique: true },
  password: String,
});

// Creamos el método estatico para el hash del password:
usuarioSchema.statics.hashPassword = function (passwordEnClaro) {
  return bcrypt.hash(passwordEnClaro, 7);
};

// Creamos un método de instanacia que comprueba la password de una usuario:
usuarioSchema.methods.comparePassword = function (passwordEnClaro) {
  return bcrypt.compare(passwordEnClaro, this.password);
};

// creamos el modelo:
const Usuario = mongoose.model('Usuario', usuarioSchema);

// exportamos el modelo
module.exports = Usuario;
