# Clase 3: Crear un nuevo agente desde le frontend.

Seguimos trabajando en la vista private.ejs:

## private.ejs:

```html
<!-- Boton para crear agentes -->
<a href="/agentes" class="btn btn-primary">New agent</a>
```

Vamos a crear un controlador para crear agentes, AgentesController.js

```javascript
class AgentesController {
  // Creamos nuevos agentes
  new(req, res, next) {
    res.render('agentes-new');
  }
}

exports.module = AgentesController;
```

Ahora en app.js creamos la ruta importando el controlador:

## app.js

```javascript
// 📌 Cargamos los controladores:
const AgentesController = require('./controllers/AgentesController');

/**
 * Rutas del website
 */
// 📌 Creamos una instancia del controlador:
const agentesController = new AgentesController();

// Ruta para crear agentes: Protegido
app.get('/agentes-new', sessionAuthMiddleware, agentesController.new);
```

Y ahora creamos en vistas la vista, vamos ameter un formulario de bootstrap:

https://getbootstrap.com/docs/5.3/forms/floating-labels/

## agentes-new.ejs:

```html
<% include cabecera.ejs %>
<!-- Basic features section-->
<section class="bg-light">
  <div class="container px-5 mt-5">
    <div
      class="row gx-5 align-items-center justify-content-center justify-content-lg-between"
    >
      <div class="col-12 col-lg-5">
        <h2 class="display-4 lh-1 mb-4"><%=__('New agent')%></h2>
        <!-- Formulario -->
        <form method="post">
          <div class=" form-floating mb-3">
            <input
              type="text"
              name="name"
              class="form-control"
              id="floatingName"
              placeholder="name@example.com"
              required
            />
            <label for="floatingInput">Name</label>
          </div>
          <div class="form-floating mb-3">
            <input
              type="number"
              name="age"
              class="form-control"
              id="floatingAge"
              placeholder="22"
              required
            />
            <label for="floatingAge">Age</label>
          </div>
          <button type="submit" class="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
</section>
<% include pie.ejs %>
```

Ahora vamos a AgenteController.js y os creamos el método para crear el agente en bd:

## AgenteController.js:

```javascript
  // Metodo para crear un agente
  async postNewAgent(req, res, next) {
    try {
      const usuarioId = req.session.usuarioLogueado;
      const { name, age } = req.body;

      const agente = new Agente({
        name: name,
        age: age,
        owner: usuarioId,
      });

      await agente.save();

      // Lo rederigimos a la pagina de privado:
      res.redirect('/private');
    } catch (err) {
      next(err);
    }
  }
```

Ahora en app.js creamos la ruta:

## app.js:

```javascript
// Ruta para crear agentes: Protegido
app.get('/agentes-new', sessionAuthMiddleware, agentesController.new);
app.post('/agentes-new', sessionAuthMiddleware, agentesController.postNewAgent);
```

Lo probamos y vemos que funciona!!!! Y vemos que cada usuario crea un usuario. Perfecto!!!
Descanso!!!

🎯 Commit: Clase 3: Creación de agentes para cada usuario en bd.
