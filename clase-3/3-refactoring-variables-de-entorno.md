# Clase 3: Refactoring del código: Variables de entorno:

Ahora vamos a hacer un refactoring del código, como vemos la cadena de coneccion con la bd esta en dos sitios ahora en app.js y tambien la tenemos en lib/connectMongoose.js lo que haremos es colocarla en un solo sitio asi no tendremos que estar cambiandola en diferentes sitios sino en uno solo.

Para eso vamos a crear un fichero de configuracion, para esto tenemos una libreria llamada dotenv: Esto lo haremos para cargar variables de entorno.

```bash
npm repo dotenv
```

https://github.com/motdotla/dotenv

Vamos a instalarla:

```bash
npm i dotenv
```

Haremos que nuestra app lea las variables de entorno.

Cambiamos en app.js y connectMongoose.js: process.env.MONGODB_URI

## app.js:

```javascript
// Cargamos el middleware de session:
app.use(
  session({
    name: 'nodeapp-session', // nombre de la cookie
    secret: 'asanjnvvldfngfsdljvfdjgfdg', // pisada de gato, esto es cualquier valor.
    saveUninitialized: true, // Forces a session that is "uninitialized" to be saved to the store.
    resave: false, // Forces the session to be saved back to the session store, even if the session was never modified during the request.
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 2, // Tiempo de expiración (ms) 2 dias, de la sesion por inactividad.
    },
    store: MongoStore.create({ mongoUrl: process.env.MONGODB_URI }),
  })
);
```

## connectMongoose.js:

```javascript
const mongoose = require('mongoose');

mongoose.connection.on('error', (err) => {
  console.log('Error de conexión', err);
});

mongoose.connection.once('open', () => {
  console.log('Conectado a MongoDB en', mongoose.connection.name);
});

// mongoose.connect('mongodb://127.0.0.1/cursonode');
mongoose.connect(process.env.MONGODB_URI);

module.exports = mongoose.connection;
```

Creamos el fichero de configuración para la variables de entorno, lo creamos en la raiz del proyecto llamado .env:

```
MONGODB_URI=mongodb://127.0.0.1/cursonode
```

Y luego al inicio de la apliacion debemos cargar la libreria dotenv: Osea en bin/www, tambien aprovechamos y la cargamos tambin en cluster, ademas tambien lo hacemos cuando cargamos os datos iniciales de la app, el archivo init-db.js:

## www:

```javascript
// Cargamos la libreria dotenv para variables de entorno:
require('dotenv').config();
```

NOTA IMPORTANTE!!! El fichero .env NO SE METE EN GIT!!!!. REVISAR QUE ESTE EN EL GITIGNORE DEL PROYECTO.
Ahora tenemos solo una cadena de conexion para la bd pero en un futuro tendre las credenciales de apis, etc. Para saber que debemos poner ahi en el archivo de varaibles de entorno de la aplicacion bien sea si esta en desarrollo o en produccion lo haremos por un lado hacer una copia del fichero .env y lo llamaremos .env.example y metemos lods datos anonemizados asi:

```
MONGODB_URI=mongodb://127.0.0.1/nombre_bd
```

Ahora en el readme.md le diremos que debe usar ese .env:

## README.md:

Copy .env.example to you custom .env.

```sh
cp .env.example .env
```

And setup your configuration.

OJO: Hemos eliminado esta linea que ya no es necesaria:

Review database connection on /lib/connectMongoose.js (see "Start a MongoDB Server in MacOS or Linux")

🎯 Commit: Clase 3: Reafactoring y creación de variables de entorno.
