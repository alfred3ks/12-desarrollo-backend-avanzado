# Clase 3: Otras formas de autenticacion:

## 1 - HTTP Basic Auth:

Ya lo vimos en el primer módulo de esenciales. Ver pdf de clase.

## 2 - API key: Lo vemos en el pdf:

La autenticación por API Key:

• La autenticación con API Key se basa en enviar en todas las peticiones un API Key (como cabecera o parámetro GET).

• Éste API Key suele ser suministrada por el servicio tras el registro de usuario.

• El API Key es único e identifica al usuario de manera unívoca.

• El API Key siempre es la misma, no cambia ni hay que renovarla. OJO esto no es del todo cierto, si se pued renovar, si se hace publica podemos renovarla en el servicio.

## 3 - Tokens (y JWT): Json web token:

![](./img/jwt.png)

Es una mezcla como de session y api key.

• JWT es un estándar abierto (RFC 7519).

• Define una forma segura y compacta de transmitir información entre diferentes partes con un objeto JSON.

• La información puede ser verificada porque va firmada digitalmente.

• Se pueden firmar los tokens con HMAC (algoritmo secreto) o con un par de claves RSA (pública/privada).

NOTA: La recomendación no usar localstorege para guardar el token como ya hemos visto antes. El recomienda hacerlo en Cookies. Ya que son mas seguras ya que usan HttpOnly, osea solo la cookie tiene acceso a mi javascript y no el resto de javascript de otras librerias.

### Cifrado asimetrico:

![](./img/jwt1.png)

### Estructura de un JWT:

Un JWT está formado por tres partes:

• Cabecera: JSON en formato base64 que indica el tipo de token y el algoritmo usado para firmarlo.

• Payload: JSON de datos en formato base64. Contiene los claims (claves del diccionario de datos).

• Firma: firma del token resultante de aplicar la siguiente fórmula: HMACRSA256( base64(header) + base64(payload) + <secret> ).

Esto es un JWT:

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkR.hcnRoIiwiX2lkIjoiczZzZDZzZHV5ZHM2N3NkNmRzIiwicm9sZSI.6ImFkbWluIiwiaWF0IjoxNTA4MDAwNTUxfQ.FfEP4sl3QGgp_HPJaCofpBSO6c6w9_MKtYuecWkvqA
```

cabecera.payload.firma

¿Por qué usar JWT?

• Evitamos guardar la sesión del usuario en la base de datos. Sobre todo por la cantidad de peticiones que se hagan a la bd, si son muchas eso puede relentizar y la experiencia puede llegar as er mala. OJO cuando hay muchas peticiones concurrentes, app como facebook, openio, etc.

• Es el usuario quien nos envía su información en todo momento.

• Es seguro, porque podemos validar que el token no ha sido alterado desde que nosotros lo enviamos.

Tenemos un web llamada jwt.io donde podemos ver como es l jwt:

https://jwt.io

🎯 Commit: Clase 3: Otras formas de autenticación.
