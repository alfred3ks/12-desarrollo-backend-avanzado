## Clase 3: Guardar sesion en bd:

Actualemente la sesion se guarda en la memoria del servidor, pero cada vez que se reinicia el servidor vemos que se reinician a cero las sesiones y tenemos que volvernos a loguearnos, lo que haremos es hacer persistencia de esa sesion en la bd.

Como ya tenemos una bd con mongodb lo haremos ahi.

Vamos a trabajajr conn el middleware de sesiones que tenemos en app.js, este middleware de sesiones tiene una propiedad store lo vemos en el repo. Por defecto lo hace en memoria como lo tenemos ahora. Para poder guardar en memoria la sesion nos apoyaremos en una libreria llamada connect-mongo.

```bash
npm repo connect-mongo
```

https://github.com/jdesboeufs/connect-mongo

La instalamos:

```bash
npm i connect-mongo
```

Lo usamos en app.js:

## app.js:

```javascript
// Requerimos el modulo para hacer persistente la sesion en bd:
const MongoStore = require('connect-mongo');

// Cargamos el middleware de session:
app.use(
  session({
    name: 'nodeapp-session', // nombre de la cookie
    secret: 'asanjnvvldfngfsdljvfdjgfdg', // pisada de gato, esto es cualquier valor.
    saveUninitialized: true, // Forces a session that is "uninitialized" to be saved to the store.
    resave: false, // Forces the session to be saved back to the session store, even if the session was never modified during the request.
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 2, // Tiempo de expiración (ms) 2 dias, de la sesion por inactividad.
    },
    store: MongoStore.create({ mongoUrl: 'mongodb://127.0.0.1/cursonode' }),
  })
);
```

Lo probamos y vemos como funciona correctamente, ya la sesion se guarda en bd. Ahora cuando vemos en la bd tenemos guardado un nuevo documento, una coleccion de sessiones.

🎯 Commit: Clase 3: Guardar sesión en bd usado libreria connect-mongo.
