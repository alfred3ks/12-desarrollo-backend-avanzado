# Clase 3: Asinación de usuario a data agentes:

Lo que veremos es que vamos a hacer que la informacion que tenemos en agentes pertenezcan a un usuario. Lo que haremos es que cuando un usuario este logueado solo se muestre su informacion no la informacion de otro usuario.

Lo que haremos es ir al modelo de Agentes.js a parte de sus propiedades tenga un propietario.

## Agentes.js:

```javascript
const agenteSchema = mongoose.Schema(
  {
    name: { type: String, index: true },
    age: { type: Number, index: true, min: 18, max: 120 },
    owner: { ref: 'Usuario', type: mongoose.Schema.Types.ObjectId },
  },
  {
    // collection: 'agentes' // para forzar un nombre concreto de colección
  }
);
```

Hemos agregado la propiedad: owner: { ref: 'Usuario', type: 'mongoose.Schema.Types.ObjectId' },

Ahora donde creamos los agentes los usuarios que es init-db.js le asignamos esa propiedad:

## init-db.js:

```javascript
// Funcion que crea agentes de inicio en la BD:
async function initAgentes() {
  // borrar todos los documentos de la colección de agentes
  const deleted = await Agente.deleteMany();
  console.log(`Eliminados ${deleted.deletedCount} agentes.`);

  // Obtenemos el id del usuario de bd:
  const [adminUser, usuario1] = await Promise.all([
    Usuario.findOne({ email: 'admin@example.com' }),
    Usuario.findOne({ email: 'usuario1@example.com' }),
  ]);

  // Le asignamos los usuarios a los agentes:
  const dataAgentes = initData.agentes.map((agente, index) => {
    if (index === 0) {
      return { ...agente, owner: adminUser._id };
    } else if (index === 1) {
      return { ...agente, owner: adminUser._id };
    } else if (index === 2) {
      return { ...agente, owner: usuario1._id };
    }
    return agente;
  });

  // crear agentes iniciales:
  const inserted = await Agente.insertMany(dataAgentes);
  console.log(`Creados ${inserted.length} agentes.`);
}
```

Hemos hecho cambio a la funcion que mete agentes a la bd, tenemos un arreglo de agentes en un .json, llamado init-data.json, al leer ese .json le estamos asignando una nueva propiedad que es el id del usuario que ya tenemos en la bd. Simplemente iteramos y devolvemos un nuevo arreglo con es información.

Asi garantizamos que esta data inicial tenga asignado un usuario inicial cuando arranquemos la app por primera vez. Lo vemos al hacer por consola:

```sh
# this command deletes all the data in the database and create default data
$ npm run init-db
```

🎯 Commit: Clase 3: Asignación de usuario a data agentes.
