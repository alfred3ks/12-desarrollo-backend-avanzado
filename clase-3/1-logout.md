# Clase 3: Página de logout:

## Bcrypt:

Vamos a hablar solo lo ultimo que hemos hecho que es el hash de las contraseñas. Existen otras librerias parecidas a bcrypt.

Esta informacion la tenemos en el pdf d ela clase:

• PBKDF2: https://keepcoding.io/blog/que-es-pbkdf2/
• bcrypt: https://codahale.com/how-to-safely-store-a-password/
• scrypt: https://github.com/Tarsnap/scrypt
• argon2: https://medium.com/@mpreziusopassword-hashingpbkdf2-scrypt-bcrypt-and-argon2-e25aaf41598e

Como elegir?

(spoiler: PBKDF2 está antiguo, cualquiera de los otros tres es bueno)

https://stytch.com/blog/argon2-vs-bcrypt-vs-scrypt/
https://hashing.dev/about/
https://npmtrends.com/argon2-vs-bcrypt-vs-scrypt-js

Vamos a continuar con la app, lo ultimo que hicimos es que en la zona privada mostraramos el email del usuario.

Vamos a realizar el logout. Y tambien haremos mejoras de la cabecera.

## Vamos a hacer el logout:

Empezaremos por la cabecera, vamos a las vistas. Vamos a hacer que las vistas puedan acceder a la session. Para eso en app.js vamos a hacerlo.

## app.js:

```javascript
// Permitimos que las vistas accedan a la session: Hacemos que el objeto session este disponible al renderizar las vistas:
app.use((req, res, next) => {
  res.locals.session = req.session;
  next();
});
```

Con esto ya tenemos. Ahora en la vista usando un condicional podremos mostrar en el menu la zona privada:

```html
<!-- Si esta logueado mostramos esto: -->
<% if(session.usuarioLogueado){%>
<li class="nav-item">
  <a class="nav-link me-lg-3" href="/private"> <%=__('Private area')%> </a>
</li>
<%}%>
```

Lo probamos y vemos que funciona. Tambien hacemos que el nboton de login se muestre solo si no esta logueado, si esta logueado mostraremnos un boton de logout.

```html
<!-- Si esta logueado mostramos esto boton de login: -->
<% if(!session.usuarioLogueado){%>
<a href="/login" class="btn btn-primary rounded-pill px-3 mb-2 mb-lg-0">
  <span class="d-flex align-items-center">
    <span class="small"> <%=__('Login')%> </span>
  </span>
</a>
<%} else {%>
<a href="/logout" class="btn btn-primary rounded-pill px-3 mb-2 mb-lg-0">
  <span class="d-flex align-items-center">
    <span class="small"> <%=__('Logout')%> </span>
  </span>
</a>
<%}%>
```

Ahora vamos a crear la funcionalidad del logout. Eso lo hacemos en el controlador LoginControler.j le pondremos un método logout:

## LoginController.js:

```javascript
  // Método para hacer logout:
  logout(req, res, next) {
    // Este método de session nos permite matar la session
    req.session.regenerate((err) => {
      if (err) {
        next(err);
        return;
      }
      res.redirect('/');
    });
  }
```

Ahora en app.js debemos asociar la ruta /logout:

## app.js:

```javascript
// Ruta para hacer logout:
app.get('/logout', loginController.logout);
```

🎯 Commit: Clase 3: Creación de la página de logout y su funcionalidad.
